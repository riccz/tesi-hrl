all: tesi.zip
pdf: tesi.pdf tesi_draft.pdf
slides: slides.pdf slides_notes.pdf

here := $(PWD)

GIT ?= $(shell which git)
ifeq ($(GIT),)
githash = ""
else
githash = $(shell $(GIT) log -1 --format=%H)
endif

.git_hash: force
	echo "$(githash)" | cmp -s - $@ || echo "$(githash)" > $@

tmpdir := $(shell mktemp -d)
tesi.zip: .git_hash tesi.pdf tesi_draft.pdf slides.pdf slides_notes.pdf
ifeq ($(GIT),)
$(error "Needs git")
else
	$(GIT) archive --format=zip --prefix=tesi/ --output=tesi.zip "$(githash)"
	mkdir -p "${tmpdir}/tesi"
	cp tesi.pdf tesi_draft.pdf slides.pdf slides_notes.pdf "${tmpdir}/tesi"
	cd "${tmpdir}" && zip -r "${here}/tesi.zip" tesi/
	rm -r "${tmpdir}"
endif

TEXFILES = tesi.tex tesi_draft.tex
TEXFILES += slides.tex slides_notes.tex
TEXFILES += chapters/*.tex
TEXFILES += frontmatter/*.tex
TEXFILES += endmatter/*.tex

BIBFILES = references.bib

FIGURES = figures/*.png figures/*.tex figures/*.pdf
FIGURES += figures/single/*.pdf
FIGURES += figures/multi/*.pdf

PKGFILES = Dissertate.cls mathcmds.sty misccmds.sty
MISCFILES = resources/*.png fonts/*.ttf endmatter/*.png

XELATEX_FLAGS=-synctex=1 -interaction=nonstopmode -file-line-error

%.pdf: %.tex ${TEXFILES} ${BIBFILES} ${FIGURES} ${PKGFILES} ${MISCFILES}
	xelatex ${XELATEX_FLAGS} "$*"
	if (test -e "$*.bcf" || grep -i 'run biber' "$*.log"); then biber "$*"; fi
	xelatex ${XELATEX_FLAGS} "$*"
	xelatex ${XELATEX_FLAGS} "$*"

clean:
	rm *.aux 2>/dev/null || true
	rm *.bbl 2>/dev/null || true
	rm *.bcf 2>/dev/null || true
	rm *.blg 2>/dev/null || true
	rm *.log 2>/dev/null || true
	rm *.nav 2>/dev/null || true
	rm *.out 2>/dev/null || true
	rm *.pdf 2>/dev/null || true
	rm *.run.xml 2>/dev/null || true
	rm *.snm 2>/dev/null || true
	rm *.synctex.gz 2>/dev/null || true
	rm *.toc 2>/dev/null || true
	rm .git_hash 2>/dev/null || true
	rm tesi.zip 2>/dev/null || true

.PHONY: all pdf clean force slides
