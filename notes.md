## Paper on D-DASH [Gadaleta 2017] ##
  ! application of deep + reinforcement learning
  - uses Deep Q-learning like [Mnih 2015]
  ? no-reference image quality (not used here) at the client
  - SSIM (at the transmitter)
  ? finite-horizon dynamic programming (check also the wireless slides)
  ? parallelizing the learning process [Chiariotti 2016]
  ! makes the channel state Markovian by keeping past samples long enough
  ! gives Q-learning convergence rate [Kearns 1999]
  ! Adaptive moment estimation (ADAM) as gradient descent [Kingma 2014]
  ! LSTM [Gers 2002] (also in the deep learning book)
  - puts the past states in the input of an MLP to keep memory
  - Two variations on deep Q-learning:
	-> many outputs (one per action)
	-> many nets with a single action
  ! uses 2 NNets:
	-> update Q(s,a) at each transition
	-> use as target a copy of Q(s,a) made every k steps

## Hierarchical learning in [Si 2004] ##
- The main points are exactly the same as in [Wiering 2012]
- More about pseudo-rewards as *local objectives*
- More algorithms both for a given hierarchy and to discover
  hierarchies
- Off-policy algorithms may be adapted to share experience between
  different sub-tasks (*intra-option learning*)
- Few examples of practical application when this was published: see
  [Crites 1996, 1998] for an example with control of elevators with
  *multiple agents*?

## HRL with multi-agents, concurrency, partial observability ##
- Extensions of MAXQ and other algs to the partially observable case
- MAXQ generalization for multi agents: use joint actions just below
  the root level (*cooperative MAXQ*)
- Distributed memory for the case of partial observability +
  multi-agent

## [djurdjevic_deep_2013] HRL with RBM ##
- Uses Conditional RBM: looks like a multilayer RBM with recurrence,
  but is not explained clearly.
- Learnt hierarchical decomposition of an MDP
- tested on a simple example (Taxi-problem-like)
- No measures of performance, no comparisons with other algorithms,
  only proves that the hierarchy can be learnt?

## [liu_multiobjective_2015] ##
- Section on HRL gives a ref [Dethlefs 2010] where HRL was used for
  text generation (large problem?)
- says that there are few examples of applications of value function
  approximation for HRL (large or continuous state-action spaces)

## [xie_joint_2016] ##
- Application to the join control of battery+supercap charge and
  cooling fan speed
- Uses two levels of RL:
  - first solves an MDP (Q-learning) that controls the fan speed and
	battery output that aims to minimize a combination of battery
	degradation and battery output, given a certain power demand. For
	many values of the tradeoff hyperparameter $\lambda$
  - this gives a curve of pareto-optimal actions
  - then solve a higher level problem with DP that picks the value of
	$\lambda$ to optimize the original objective function (CWC ->
	total workload over the lifetime)
