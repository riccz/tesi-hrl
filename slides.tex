\documentclass[xetex]{beamer}

\usepackage[T1]{fontenc}
\usepackage[english]{babel}

\definecolor{SchoolColor}{RGB}{155, 0, 20} % UNIPD red
\definecolor{chaptergrey}{rgb}{0.61, 0, 0.09} % dialed back a little
\definecolor{midgrey}{rgb}{0.4, 0.4, 0.4}
\usetheme{CambridgeUS}
\colorlet{darkred}{SchoolColor} % Overwrite the red from 'beaver'
\setbeamercolor*{structure}{fg=SchoolColor,bg=white}
\setbeamercolor{alerted text}{fg=chaptergrey}
\setbeamercolor*{title}{fg=white,bg=SchoolColor}
\useinnertheme{circles}

\usepackage{mathcmds}
\usepackage{misccmds}

%% \usepackage[%
%%   dashed=false,%
%%   backend=biber,%
%%   maxbibnames=3,%
%%   sorting=none,%
%%   style=ieee,%
%% ]{biblatex}
%% \addbibresource{references.bib}

\usepackage[per-mode=symbol, detect-all, exponent-product=\cdot]{siunitx}
\usepackage[super]{nth}

\usepackage{tikz}
\usetikzlibrary{%
        automata,%
        backgrounds,%
        calc,%
        chains,%
        decorations.markings,%
        decorations.pathreplacing,%
        patterns,%
        positioning,%
}

\newlength{\figwidth}
\newlength{\subfigwidth}
\setlength{\figwidth}{0.66\textwidth}
\setlength{\subfigwidth}{0.495\textwidth}

\usepackage{graphicx}

% \usepackage{import}
%\usepackage{epstopdf}
\usepackage{hyperref}
\usepackage{color}
\usepackage{subcaption}
%\usepackage[section, above, below]{placeins}

\usepackage{booktabs}




\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
\renewcommand\algorithmicthen{}
\renewcommand\algorithmicdo{}
\makeatletter
%% \@addtoreset{algorithm}{chapter}% algorithm counter resets every chapter
\usepackage{lscape}
%% \renewcommand{\thealgorithm}{\thechapter.\arabic{algorithm}}% Algorithm # is <chapter>.<algorithm>


\algnewcommand{\IIf}[1]{\State\algorithmicif\ #1\ \algorithmicthen}
\algnewcommand{\EndIIf}{\unskip\ }


\usepackage[nolist]{acronym}

\newif\if@in@acrolist
\AtBeginEnvironment{acronym}{\@in@acrolisttrue}
\newrobustcmd{\LU}[2]{\if@in@acrolist#1\else#2\fi}

\newcommand{\ACF}[1]{{\@in@acrolisttrue\acf{#1}}}
\newcommand{\ACL}[1]{{\@in@acrolisttrue\acl{#1}}}
\makeatother


\AtBeginSection[]{
  \begin{frame}
    \begin{block}{}
      \centering%
      \usebeamercolor[fg]{section title}%
      \usebeamerfont{section title}%
      \insertsection
    \end{block}
  \end{frame}
}

\defbeamertemplate*{title page}{customized}[1][]{
  {
    \setbeamercolor{block body}{parent={title}}
    \usebeamerfont{title}
    \begin{block}{}
      \vspace{6pt}
      \begin{columns}
        \begin{column}{0.66\textwidth}
          \inserttitle\par
        \end{column}
        \begin{column}{0.15\textwidth}
          \includegraphics[width=\textwidth]{resources/unipd-white}
        \end{column}
      \end{columns}
      \vspace{6pt}
    \end{block}
  }
  \vspace{18pt}
  \makebox[\textwidth][c]{
    \begin{minipage}{\textwidth-18pt}
      \usebeamerfont{author}
      \pbox{0.5\textwidth}{\textit{Supervisor} \\ Prof. Andrea Zanella}
      \hfill
      \pbox{0.5\textwidth}{\textit{Candidate} \\ \insertauthor\relax}
      \par
      \vspace{12pt}
      \pbox{0.5\textwidth}{\textit{Co-supervisor} \\ Dott. Federico Chiariotti}
    \end{minipage}
  }
  \vfill
  {
    \centering
    \usebeamerfont{institute}\insertinstitute\par
    \vspace{6pt}
    \usebeamerfont{date}\insertdate\par
  }
  %% \usebeamercolor[fg]{titlegraphic}\inserttitlegraphic
}

\usepackage{pbox}

\title[MARL for UAV patrolling]{Multi-agent reinforcement~learning %
  for self-coordinated~UAVs~patrolling}
\author{Riccardo Zanol}
\date{1 October 2018}
\institute[]{%
  Universit\`a di Padova \\ %
  Dipartimento di Ingegneria dell'Informazione%
}

\providecommand\notesopts{hide notes}
\expandafter\setbeameroption{\expandafter\notesopts}

\begin{document}
\begin{frame}[plain]
  \titlepage\relax
  \note{}
\end{frame}

\section*{Introduction}
\begin{frame}{Introduction}
  \begin{itemize}
  \item 3D mapping with drones
  \item High-level path planning
  \item RL/MARL
  \item Assume another algorithm for the actual 3D reconstruction
  \item Learn to fly to unknown areas
  \item Battery constraints
  \end{itemize}
\end{frame}

\begin{frame}{Reinforcement Learning}
  \begin{figure}
    \centering
    \includegraphics[width=0.66\textwidth]{figures/RL_sutton}
    \caption{Single-agent RL.\@ From~[15]}
  \end{figure}
  \begin{itemize}
  \item If $(R_{t+1}, S_{t+1})$ Markovian:  Markov Decision Process (MDP)
  \item Here: continuous state, finite actions
  \item Transition function $T(s,a,s') \mapsto [0,1]$
  \item Reward function $r(s,a,s') \mapsto \realset$
  \end{itemize}
  \note{}
\end{frame}

\begin{frame}{Partially Observable MDP}
  \begin{columns}
    \begin{column}{0.33\textwidth}
      \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{figures/POMDP_wiering}
        \caption{RL with partial observability. From~[19]}
      \end{figure}
    \end{column}
    \begin{column}{0.66\textwidth}
      \begin{itemize}
      \item When the agent does not know the state
      \item \alert{Observations} of the actual state
      \item Transition function as before
      \item Reward function as before
      \item Observation function $O(s', a, o) \mapsto [0,1]$
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Multi-agent RL}
  \begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{figures/MARL_wiering}
    \caption{Multi-agent RL.\@ From~[17]}
  \end{figure}
  \begin{itemize}
  \item Joint action, separate rewards
  \item Markovian if the agents coordinate the joint action
  \item Independent Learners (not Markovian)
  \end{itemize}
\end{frame}

\section{RL algorithms}

\begin{frame}{Q-Learning}
  \begin{minipage}{\textwidth}
    \vspace{1.5cm}
    \scriptsize
    \begin{algorithmic}
      \Require \parbox[t]{5cm}{discount rate $\gamma$, \\ learning rate $\alpha(k,s,a)$, \\
        exploration policy $\tilde{\pi}_k(s,a)$}
      \State initialize $Q(s,a)$
      \For{\textbf{each} episode}
      \State $s \gets$ starting state
      \State $k \gets 1$
      \Repeat
      \State choose $a \in \mathcal{A}(s)$ with probability $\tilde{\pi}_k(s, a)$
      \State make a transition from $s$ with action $a$
      \State let $r$ be the reward, $s'$ be the next state
      \usebeamercolor*[fg]{alerted text}
      \State $Q(s,a) \gets (1 - \alpha(k,s,a)) Q(s,a) +
      \alpha(k,s,a)\left( r + \gamma \max_{a'}Q(s', a') \right)$%
      \usebeamercolor*[fg]{normal text}%
      \State $s \gets s'$
      \State $k \gets k+1$
      \Until{$s$ is terminal}
      \EndFor
    \end{algorithmic}
  \end{minipage}
  \begin{tikzpicture}[remember picture, overlay]
    \node [anchor=north east, shift={(0,-1.5cm)}]  at (current page.north east) {%
      \parbox{0.5\textwidth}{
        \begin{itemize}
        \item Action value function $Q$
        \item Off-policy
        \item Temporal Difference
        \item Converges on \alert{finite, stationary} MDPs
        \end{itemize}
      }
    };
\end{tikzpicture}
\end{frame}

\begin{frame}{Q-Learning extensions}
  \begin{itemize}
  \item Same structure, different learning rate
  \item The new learning rate \alert{depends on $\tilde{\pi}_k$}: \\ update more
    aggressively rare $(s,a)$ pairs
  \item Two versions:
    \begin{itemize}
    \item Frequency-Adjusted QL (FAQL): $\alpha_\mrm{FAQL}(k, s, a)
        = \alpha(k, s,a) \min\left(\frac{\beta(k, s,
          a)}{\tilde{\pi}_k(s, a)}, 1\right)$
      \item Needs $\beta$ to avoid an unbounded learning rate
      \item Repeated Update QL (RUQL): $ \alpha_\mrm{RUQL}(k, s, a)
        = 1 - (1 - \alpha(k, s, a))^\frac{1} {\tilde{\pi}_k(s, a)}$
      \item Removes $\beta$: no degeneration to QL for rare $(s,a)$ pairs
    \end{itemize}
  \item Convergence on finite, stationary MDPs adapted from QL
  \item Work also on non-stationary problems
  \end{itemize}
\end{frame}

\begin{frame}{Model-based RL}
  \begin{itemize}
  \item How much worse if the transition function is unknown?
  \item Asynchronous Value Iteration:
    \begin{itemize}
    \item Similar to QL, but uses the known transition function
    \item Planning algorithm
    \item Examines the transition probabilities of the next $n$ steps
    \item $n$-step greedy lookahead
    \end{itemize}
  \item Monte Carlo Tree Search (MCTS):
    \begin{itemize}
    \item Sparse version of Lookahead
    \item mix of Monte Carlo rollouts and regularized greedy policy
    \item examine only the most valuable paths
    \item uses a generative model instead of the transition function
    \end{itemize}
  \end{itemize}
\end{frame}

\section{MDP Model of the UAVs problem}

\begin{frame}{3D reconstruction}
  \begin{itemize}
  \item Next Best View problem
    \begin{itemize}
    \item Mobile visual sensor
    \item Choose (greedily) where to move to take the next image
    \end{itemize}
  \item Reconstruction quality vs. time using a drone
  \item Approximately exponential
  \item Model as a \alert{2-state Markov Chain}
  \item Cumulative reward is exponential (for a small number of steps)
  \end{itemize}
  \begin{figure}
    \includegraphics[width=0.5\textwidth]{figures/3d_model}
  \end{figure}
\end{frame}

\begin{frame}{Battery}
  \begin{itemize}
  \item Power consumption: Gaussian with varying parameters
  \item Battery discharge: non-linear voltage source with internal $R$
  \end{itemize}
  \begin{equation*}
    \begin{aligned}
      V_\mathrm{batt}(t) &= E(t) - R I_\mathrm{batt}(t) \\
      E(t) &= E_0 - K \frac{Q}{Q - \currint} + A e^{-B \currint}
    \end{aligned}
  \end{equation*}
  \begin{columns}
    \begin{column}{0.4\textwidth}
      \begin{itemize}
      \item Combined power and discharge models
      \item Simulated with IID actions
      \item Linear discharge: avoid simulation
      \end{itemize}
    \end{column}
    \begin{column}{0.6\textwidth}
      \begin{figure}
        \includegraphics[width=0.8\textwidth]{figures/battery_gaussian_Tshort}
      \end{figure}
    \end{column}
  \end{columns}
\end{frame}


\begin{frame}{MDP}
  \begin{itemize}
  \item Drones $\to$ agents
  \item Independent Learning: each agent sees a non-stationary MDP
  \end{itemize}
  \begin{equation*}
    T_i(s, a_i, s') = \sum_{\vec{a_{-i}} \in \mathcal{A}^{\Nag-1}}
    T\left(s, (\vec{a_{-i}}, a_i), S'\right)
    \prod_{j \neq i} \pi_j(s, a_j)
  \end{equation*}
  \begin{itemize}
  \item Target area divided into cells
  \item Agents move deterministically to adjacent cells, or run NBV on their cell
  \item Empty battery: action fails and agents must return to recharge
  \item Reward given by independent 2-state MC, one for each cell, when an agent ``stays''
  \end{itemize}
\end{frame}

\begin{frame}{MDP}
  \begin{figure}
    \resizebox{0.66\textwidth}{!}{\input{figures/grid_example.tex}}
  \end{figure}
  \begin{itemize}
    \item 4 state variables per agent: $(x_i, y_i, b_i,
      \alert{a_i^{(-1)}})$
    \item Previous action: avoid going back
    \item Agents with empty battery: forced transitions toward the
      start
  \end{itemize}
\end{frame}


\begin{frame}{Optimistic upper bound}
  \begin{itemize}
  \item Assumptions:
    \begin{enumerate}
    \item There is always an ON cell to enter in
    \item The battery becomes empty only when exiting a cell
    \item Reset all cells to ON after a recharge
    \item Return to start in 1 step
    \end{enumerate}
  \item Renewal-reward process
  \item Long term average reward: $\lim_{n \to \infty} \frac{\E{R(n)}}{n} \leq 0.701$
  \item Valid for a single agent
  \item Valid for each agent in the multi-agent case \\ (but much more optimistic)
  \end{itemize}
\end{frame}

\begin{frame}{Single-agent POMDP}
  \begin{itemize}
  \item Full MDP is too complex for QL (and also continuous)
  \item Two problems: battery and grid state
  \item Battery:
    \begin{itemize}
    \item Must be discretized
    \item Decreases slowly (w.r.t $T$)
    \item Geometrically increasing steps: represent better an almost
      empty battery
    \end{itemize}
  \item Grid state:
    \begin{itemize}
    \item size exponential in the number of cells
    \item observe one cell: the one under the agent
    \item now the previous action is actually important
    \end{itemize}
    \item Full single-agent state: 5 variables $(x,y,b,a^{(-1)},z_{x,y})$
  \end{itemize}
\end{frame}

\begin{frame}{Multi-agent POMDP}
  \begin{itemize}
  \item Multiple agents: state scales exponentially even if simplified
  \item Observe only the closest agent: quantized distance and direction
  \item Full multi-agent state: 7 variables $(x,y,b,a^{(-1)},z_{x,y},d, \theta)$
  \end{itemize}
\end{frame}


\section{Results}

\begin{frame}{Parameters}
  \begin{itemize}
  \item Fix shape of $\alpha, \beta$: decrease as $1/t^C$
  \item Fix shape of $\epsilon$ or $\tau$: linear decrease
  \item 6 parameters (8 for FAQL)
  \item Random grid search for max average reward
  \end{itemize}

\end{frame}

\begin{frame}{Testing process}
  \begin{itemize}
  \item Same agents for 2~million steps
  \item Extract $Q$ at 24 points
  \item Test on \num{10000}~steps
  \item Average over 200 runs
  \end{itemize}
\end{frame}

\begin{frame}{Single-agent training times}
    \begin{columns}
      \begin{column}{0.5\textwidth}
          \begin{figure}
            \centering
            \includegraphics[width=\textwidth]{figures/single/train_times}
          \end{figure}
      \end{column}
      \begin{column}{0.5\textwidth}
        \begin{itemize}
        \item Training step = simulation + update rule
        \item RUQL/FAQL slower than QL
        \item Lookahead does not learn
        \end{itemize}
      \end{column}
    \end{columns}

    \note[item]{FAQL is slower because of implementation details: same
      update rule, different learning rate}

    \note[item]{Lookahead does not learn}
\end{frame}

\begin{frame}{Single-agent testing times}
    \begin{columns}
      \begin{column}{0.5\textwidth}
        \begin{figure}
            \centering
            \includegraphics[width=\textwidth]{figures/single/test_times_log}
        \end{figure}
      \end{column}
      \begin{column}{0.5\textwidth}
        \begin{itemize}
        \item Testing step = simulation + policy
        \item Lookahead is \alert{300 times} slower
        \end{itemize}
      \end{column}
    \end{columns}
    \note{}
\end{frame}

\begin{frame}{Single-agent average reward}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{figures/single/avg_reward_all}
      \end{figure}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{itemize}
      \item QL-based all converge around 0.5/step
      \item Convergence speed: RUQL is best (\num{600000} steps),
        FAQL better than QL
      \item FAQL starts with high variance
      \item Close to Lookahead (and bound)
      \end{itemize}
    \end{column}
  \end{columns}
\note{}
\end{frame}

\begin{frame}{Multi-agent training times}
    \begin{columns}
      \begin{column}{0.5\textwidth}
          \begin{figure}
            \centering
            \includegraphics[width=\textwidth]{figures/multi/train_times}
          \end{figure}
      \end{column}
      \begin{column}{0.5\textwidth}
        \begin{itemize}
        \item Like single-agent
        \item RUQL/FAQL are slower: 5.5 vs 5 ms
        \item MCTS plans each step
        \end{itemize}
      \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Multi-agent testing times}
    \begin{columns}
      \begin{column}{0.5\textwidth}
        \begin{figure}
            \centering
            \includegraphics[width=\textwidth]{figures/multi/test_times_log}
        \end{figure}
      \end{column}
      \begin{column}{0.5\textwidth}
        \begin{itemize}
        \item MCTS even slower than Lookahead: \alert{2700 times}
        \item QL-based: $<4$ times w.r.t. single-agent
        \end{itemize}
      \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Multi-agent average reward}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{figures/multi/avg_reward_all}
      \end{figure}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{itemize}
      \item Scale: much more similar
      \item Same convergence rate (1.5~million steps)
      \item All lower w.r.t. upper bound (\SI{60}{\percent})
      \end{itemize}
    \end{column}
  \end{columns}
\note{}
\end{frame}

\section*{Conclusions}
\begin{frame}{Conclusions}
  \begin{itemize}
  \item QL-based algorithms can be applied successfully to this scenario
  \item Loss due to not knowing the transition function is small
  \item QL-based are much faster than the two model-based algorithms
  \item Single-agent: reasonably close to the upper bound
  \end{itemize}
\end{frame}

\begin{frame}[plain]
  \centering \Huge
  \usebeamercolor[fg]{structure}
  \emph{Thank You}
\end{frame}

\end{document}
