% !TEX root = ../tesi.tex

\chapter{Single-agent results}\label{ch:results_sa}
In this chapter, the multiple-MDPs model from Chapter~\ref{ch:model}
will be further simplified to reduce the size of state space to a
manageable size and the results of the simulations for a single agent
will be discussed. The single-agent MDP will be solved with an
$n$-step greedy lookahead algorithm, with Q-learning and two of its
extensions (FAQL and RUQL) and their performance will be compared.

\section{Single-agent POMDP}\label{sa:pomdp}
In the case of a single agent, there is only one of the related MDPs
$\left(\mathcal{S},\mathcal{A},T_i,R_i \right)$ from
Section~\ref{mod:mdp}, where $\mathcal{S} = \mathcal{S}_\mathrm{ag}
\times \mathcal{Z}$.  So the state variable can be in one of the
following two forms:
\begin{equation}
    s = \begin{cases}
      \left(x,y,b,a^{(-1)}, Z \right),
        x \in \mathcal{X},
        y \in \mathcal{Y},
        b \in \mathcal{B},
        a^{(-1)} \in \mathcal{A},
        Z \in \mathcal{Z} \\
      \left(\mrm{ret},Z\right),
        \mrm{ret} \in \set{0, 1, \dots, M_\mrm{ret}-1},
        Z \in \mathcal{Z}
    \end{cases}
    .
\end{equation}
In this case, the MDP's transition function is stationary, since there
are no other agents that can influence the state transitions with
their action.

In order to apply the tabular RL algorithms described in
Chapter~\ref{ch:reinf}, the MDP needs to be \emph{finite}. Since the
only continuous variable is $b \in [0,1]$, the state space can be
discretized by replacing $b$ with $\tilde{b} \in \tilde{\mathcal{B}} =
\set{\tilde{B}_0, \tilde{B}_1, \dots, \tilde{B}_{M_b - 1}}$.  The
quantization, however, makes the battery process lose the Markov
property; this can be seen by considering that $b(k) - b(k+1) =
\DeltaSOC(k+1)$ is non-negative, except at times when the battery is
recharged, and, therefore, the probability $\ProbC{\tilde{b}(k+1) =
  \tilde{b}}{x,y,a,\tilde{b}(k) = \tilde{b}}$ of staying at the same
level decreases at each successive $\tilde{b} \to \tilde{b}$
transition.

Since the SOC decrease rate is slow with respect to the step time, a
large number $M_b$ of levels would be needed in order to accurately
represent the actual value of $b$. However, this can become
computationally expensive.

It can be noticed that the battery level matters the most to the agent
when the battery is emptying, because it has to decide whether to go
back to recharge or to continue its mission. The battery values will
be thus quantized non-uniformly in order to represent the lower values
more accurately.
%
A smaller quantization step near the empty-battery state should make
the evolution of the SOC ``more Markovian'' as the battery level
decreases, thus allowing the agent to better distinguish the effects
of its actions.

The chosen quantization step increases geometrically from 0 toward 1,
and the smallest interval is chosen to be smaller (with probability
$p \geq 1 - 10^{-9}$) than any possible value of $\DeltaSOC$.

Let $\set{u_0, u_1, \dots, u_{M_b - 1}}$ be the upper bounds of the
quantization intervals, let $\set{l_0, l_1, \dots, l_{M_b - 1}}$ be
the corresponding lower bounds ad let $\tilde{B}_i = \frac{l_i +
  u_i}{2}, i = 0,\dots,M_b-1$ (see Figure~\ref{fig:quant_names}).
\begin{figure}[htbp]
  \centering
  \input{figures/quant_names.tex}
  \caption{Notation used in the quantization of the battery state.}
  \label{fig:quant_names}
\end{figure}
The quantization intervals are chosen such that they satisfy the
following conditions:
\begin{equation}
  \begin{aligned}
    u_0 &= \min\Delta_\mathrm{SOC} \\
    \frac{u_i}{u_{i-1}} &= C \quad i = 1,\dots,M_b-1 \\
    u_{M_b - 1} &=1 ,
  \end{aligned}
\end{equation}
which give the quantization intervals shown in
Figure~\ref{fig:quantized_soc}.
\begin{figure}[htbp]
  \centering
  \includegraphics[width=\figwidth]{figures/quantized_soc}
  \caption{Non-uniform quantization levels for the SOC. The number of
   intervals is $\msf{M_b = 10}$ and the smallest interval is
    $\msf{\min\Delta_\mathrm{SOC} = 0.016 }$.}
  \label{fig:quantized_soc}
\end{figure}

The state variable for the battery SOC $b$ will be tracked
continuously in the underlying MDP, but the agent will only see the
quantized value $\tilde{b}$.  This makes the random process seen by
the agent a \ac{POMDP} $\left(\mathcal{S}, \Omega, \mathcal{A}, T, O,
R\right)$, where the state space, action space, transition function
and reward function are the same as those from the underlying MDP.

In addition to the quantization of the battery state, it is useful to
give the agent a reduced version of the grid state. Firstly because it
may not be realistic for the agent to know the reward process'~state
for the entire grid; and secondly because of the size of $\mathcal{S}$
with the full grid.  The single-agent state space has a size
\begin{equation}
  \abs{\mathcal{S}} = \abs{\mathcal{Z}} \left( \abs{\mathcal{S_\mrm{grid}}}
   + \abs{\mathcal{S}_\mrm{ret}}\right) =
   2^{M_xM_y} \left( M_x M_y M_b \abs{\mathcal{A}} + M_\mrm{ret}\right)
\end{equation}
that scales exponentially in the number of cells $M_xM_y$, so it
becomes infeasible to apply tabular RL methods if the environment is
large.  The simplification of $Z(k)$ into $z_{x(k),y(k)}(k)$ avoids
this problem and still lets the agent know the state it is currently
passing over.

The loss of knowledge about the entire grid state also makes the
presence of the previous action $a^{(-1)}$ in the state actually useful.
%
If the agent had knowledge of the full grid state $Z(k)$, it would not
need to remember the last cell is visited, which is likely to be OFF,
because it would already know whether that cell had turned ON again or
not.

The observation space $\Omega$ of the single-agent POMDP is
\begin{equation}
  \begin{aligned}
    \Omega=&\left(\set{\OFF, \ON}\times\set{(x, y, \tilde{b}, a^{(-1)})} \right)
      \\ &\cup \set{0, 1, \dots, N_\mrm{ret}-1}, \\
  & x \in \mathcal{X}, y \in \mathcal{Y}, \tilde{b} \in \tilde{\mathcal{B}},
  a^{(-1)} \in \mathcal{A}
  \end{aligned}
  \label{eq:obs_space}
\end{equation}
and the corresponding observation function is deterministic:
\begin{equation}
  O(s', a, o) = \begin{cases}
    \ind{ o = s' } &\text{if}\; s' \in \mathcal{S}_\mrm{ret} \\
    \ind{ o = (z_{x',y'}, x', y', \argmin_{\tilde{B_i}}\abs{\tilde{B_i} - b}, a)}
    &\text{otherwise}
  \end{cases}
  .
\end{equation}

\section{Reward upper bound}
A way to compute an upper bound on the average reward that the agent
can collect is to consider the single-agent MDP with the following
optimistic assumptions:
\begin{enumerate}
  \item the agent can always move into an \ON
    cell; \label{enum:alwayson}
  \item the battery can become empty only when the agent is moving out from a
    cell;\label{enum:emptyonmove}
  \item the reward process resets every cell to \ON when the agent
    returns to the origin.\label{enum:reseton}
\end{enumerate}
With these assumptions, the single-agent MDP reduces to a simpler
renewal-reward process: the agent starts with a full battery, it moves
into an \ON cell, it stays there for $N$ steps, until the first \OFF
state, and then moves to another cell. This process of visiting a cell
repeats $K$ times, until the battery level reaches zero; at this point,
the agent returns to recharge in $M$ steps and the process begins
again.  Let the total length of the $n$-th renewal cycle be $T_n$,
which can be written as
\begin{equation}
  T_n = \sum_{k=1}^{K_n} \left( 2 + N_{k,n} \right) + M_n
  ,
  \label{eq:renewal_cycle}
\end{equation}
where the number of cells visited is
\begin{equation}
  K_n = \argmin_k \left( \sum_{j=1}^k \DeltaSOC_{j, n} \geq 1 \right)
  \label{eq:def_Kn}
\end{equation}
and the battery charge consumed during a cell visit, due to the single
move action and the repeated stay actions, is
\begin{equation}
  \DeltaSOC_{j, n} = \DeltaSOC_{j,n}^{(m)} +
    \sum_{l=1}^{N_{j,n} + 1} \DeltaSOC_{l,j,n}^{(h)}
    ,
\end{equation}
and let the corresponding total reward be
\begin{equation}
  R_n = \sum_{i=1}^{K_n} N_{i,n}
  \label{eq:cycle_reward}
  .
\end{equation}
The event that corresponds to $K_n$ taking a certain value can be
written explicitly as:
\begin{equation}
  \begin{aligned}
  \set{K_n = k} = &\left\{ \sum_{j=1}^k \left( \DeltaSOC_{j,n}^{(m)} +
  \sum_{l=1}^{N_{j,n} + 1} \DeltaSOC_{l,j,n}^{(h)} \right) \geq 1, \right. \\
  & \left. \sum_{j=1}^{k-1} \left( \DeltaSOC_{j,n}^{(m)} +
  \sum_{l=1}^{N_{j,n} + 1} \DeltaSOC_{l,j,n}^{(h)} \right) < 1 \right\}
  \end{aligned}
\end{equation}
and it can be seen that $\set{K_n = k}$ is independent both of
$N_{j,n}$ and of $\DeltaSOC_{j,n}$ $\forall j > k$. Therefore $K_n$ is
a stopping time for $N_{j,n}$ and $\DeltaSOC_{j,n}$, hence Wald's
equation (see~Theorem~3.3.2~from~\cite{ross_stoch}) applies to both
the random sums in \eqref{eq:renewal_cycle} and
\eqref{eq:cycle_reward}.
%
It also applies to $\sum_{j=1}^{K_n} \DeltaSOC_{j, n}$, which allows
to compute a bound on the average number of visited cells, starting
from~\eqref{eq:def_Kn}:
\begin{alignat}{2}
  1 \leq & \sum_{j=1}^{K_n} \DeltaSOC_{j, n} &&< 1 + \DeltaSOC_{K_n,n} \\
  1 \leq & \E{\sum_{j=1}^{K_n} \DeltaSOC_{j, n}} &&< 1 + \E{\DeltaSOC_{K_n,n}} \\
  1 \leq & \E{K_n} \E{\DeltaSOC} &&< 1 + \E{\DeltaSOC} \\
  \frac{1}{\E{\DeltaSOC}} \leq& \E{K} &&< \frac{1}{\E{\DeltaSOC}} + 1 ,
  \label{eq:avgKbound}
\end{alignat}
where the average battery consumption per cell visit is
\begin{equation}
  \E{\DeltaSOC} = \E{\DeltaSOC^{(m)}} +
  \E{\sum_{l=1}^{N_{j,n} + 1} \DeltaSOC_{l,j,n}^{(h)}} =
  \mu_m + \mu_h (1 + \E{N}) ,
\end{equation}
since $\DeltaSOC_{l,j,n}$ and $N_{j,n}$ are all independent.
%
The average cycle length can also be computed as
\begin{equation}
  \E{T} = \E{K}(2+\E{N})  + \E{M}
\end{equation}
% which can be bounded using \eqref{eq:avgKbound}:
% \begin{equation}
%   \frac{2 + \E{N}}{\E{\DeltaSOC}} + \E{M} \leq \E{T} <
%   \frac{(2 + \E{N})(1 + \E{\DeltaSOC})}{\E{\DeltaSOC}} + \E{M}
%   ,
% \end{equation}
while the average reward per cycle is
\begin{equation}
  \E{R} = \E{N}\E{K}
  .
\end{equation}

Now, using Theorem~3.6.1~from~\cite{ross_stoch}, the long-term
average reward (per slot of the MDP) is
\begin{equation}
  \lim_{n \to \infty} \frac{\E{R(n)}}{n} = \frac{\E{R}}{\E{T}} =
  \frac{\E{N}}{2+\E{N}  + \frac{\E{M}}{\E{K}}} ,
\end{equation}
and using the upper bound from \eqref{eq:avgKbound},
\begin{equation}
  \lim_{n \to \infty} \frac{\E{R(n)}}{n} \leq
  \frac{\E{N}}{2 + \E{N} + \frac{\E{\DeltaSOC}\E{M}}{1 + \E{\DeltaSOC}}}
  .
\end{equation}
An optimistic, but still realistic, assumption for the distribution of
the number of return steps is $M_n = 1,\forall n$. It is realistic,
because the agent should learn to stay closer and closer to the origin
as its battery level decreases; ideally, the battery should deplete
while the agent is staying over one of the four cells adjacent to the
origin.  With this assumption regarding the return steps and the
values for the average \ON steps and the average battery consumption
given in Chapter~\ref{ch:model}, the upper bound is
 \begin{equation}
  \lim_{n \to \infty} \frac{\E{R(n)}}{n} \leq 0.701
  .\label{eq:single_bound}
 \end{equation}

\section{Simulation results}
The single-agent scenario was simulated as defined above and in
Chapter~\ref{ch:model} to determine the performance of an agent that
follows one of the following algorithms:
\begin{itemize}
  \item $n$-step greedy lookahead,
  \item Q-Learning,
  \item FAQL,
  \item RUQL.
\end{itemize}

\subsection{Lookahead}
In this case, the agent does not attempt to learn anything from its
interaction with the environment. It is assumed to know the transition
probability function $T(s, a, s')$ and it uses it to choose the action
that will give the best average discounted rewards at each time step,
planning $n$ steps ahead.

Even considering the quantized battery variable in place of the actual
one, however, it is very computationally expensive to plan even few
steps ahead due to the large number of next states $s'$ that the agent
can find itself in with non-zero probability,

In order to make the planning faster, instead of the next states, the
agent will consider the next \emph{observation} probabilities. The
computation of the next possible observation still requires an
enumeration of all possible next states:
\begin{equation}
  \ProbC{o(k+1) = o'}{s(k) = s, a(k) =a} =
  \int_\mathcal{S} O(s', a, o') T(s, a, s')\mrm{d}s' , \label{eq:obsinteg}
\end{equation}
where the integral can easily be computed given $(s, a, o')$,
because either $s \in \mathcal{S}_\mrm{ret}$, and the only randomness comes from
 the (discrete) distribution of $Z'$, or the observation contains
 $\tilde{b}' \in [l', u']$ and the integral becomes
 \begin{align}
  \int_{l'}^{u'} & \sum_\mathcal{Z} O(s', a, o') T^{(Z)}(s, a, Z') \\
  & \cdot \ind{(x,y,a^{(-1)}) \text{ becomes }(x', y', a)\text{ given }a}
  T^{(b)}(s, a, b') \mrm{d}b' ,
 \end{align}
which can be computed using the (Gaussian) CDF of $\DeltaSOC$.

Even if the observation probability computation still requires to
enumerate all next states, the computational cost is reduced when
planning more than one step ahead because there are fewer starting
points to consider for the next steps.

The probability in~\ref{eq:obsinteg} depends still on the state
$s(k)$, hence this approach may not be useful if the lookahead
algorithm needs to consider more than one step from the current state.
%
Considering the state variables that are not preserved by the
observations, it can be seen that they do not affect the agent too
much if the planning horizon is short. The battery level quantization
is non-uniform specifically for this purpose, since the battery level
variations are more important near the end.  The grid state also
matters only in the immediate vicinity of the agent, since it can move
only one step at a time.
%
It is, thus, possible to work around the limitation described above by
expanding the observations into full states. The missing information
is copied from the previous known state, in the case of the cell
states, while the battery state is tracked using only the quantized
SOC value.

Figure~\ref{fig:single_avg_reward} shows the average reward per step
gained over 50 episodes of \num{10000} steps each. The discount rate
was set to $\gamma = 0.6$ and the planning is stopped three steps
ahead of the agent's state.  The plots show that the lookahead
algorithm gives a reasonably close performance  (\SI{14}{\percent} less)
to the optimistic bound: the average reward during the episodes was
\num{0.602} with a standard deviation of \num{5.48e-3}.

\subsection{Q-Learning-based algorithms}
The three Q-Learning-based algorithms were run over the POMDP: they use
the observations as if they were the actual states.  Each algorithm
has a handful of parameters to choose: all of them need a discount
rate $\gamma$, a learning rate for each step $\alpha(k)$ and the
parameters relative to the exploration policy.  The two exploration
policies considered in the simulation are the $\epsilon$-greedy policy
and the softmax policy and each of them needs an additional parameter
for each time step: the probability of making a random choice
$\epsilon(k)$ or the softmax temperature $\tau(k)$.
%
In addition to these, FAQL needs another ``learning-rate-like''
parameter $\beta(k)$ to control the effect of the state visitation
frequency on the update to the value function.

Since the three algorithms are applied unmodified to the POMDP, none
of the convergence theorems cited in Sections~\ref{reinf:qlearn}
and~\ref{reinf:qlearn_ext} actually hold because of the transition
non-stationariety.  Despite this, they are still useful, because they
provide some guidance during the selection of the parameters and
exploration policies.  In practice, FAQL and RUQL have a good
performance even in non-stationary environments
\cite{abdallah_addressing_2016}, as they were designed to do. %% The
%% authors of FAQL \cite{kaisers_frequency_2010} also proved its
%% convergence in a simpler two-agents stateless environment.

% \todo{ Convergence proofs in \cite{singh_convergence_2000}: GLIE
% (greedy in the limit with infinite exploration) decaying
% exploration: every action is exectuted infinite times in states that
% are visited infinite times, in the limt the policy is greedy wrt $Q$
% wp 1.

% softmax and epsilon-greedy are GLIE, BUT not with the parameters
% used here

% \cite{singh_convergence_2000} proves convergence of SARSA(0).  Read
% for citations reg. QL of proof applicability }

The hyper-parameter optimization process was a mix of random grid
searching and manual tuning. After fixing the shapes of learning rates
and exploration policies to be those defined in (\ref{eq:ql_alpha},
\ref{eq:faql_beta}, \ref{eq:epsilon} and \ref{eq:tau}), the
hyper-parameter space can be one of the following:
\begin{equation}
  \begin{aligned}
    &\set{\gamma, \alpha_0, \alpha_\mrm{exp},
      \epsilon_0, \epsilon_C, \epsilon_\mrm{min}}
    && \text{(RU)QL, $\epsilon$-greedy} \\
    &\set{\gamma, \alpha_0, \alpha_\mrm{exp},
      \tau_0, \tau_C, \tau_\mrm{min}}
    && \text{(RU)QL, softmax} \\
    &\set{\gamma, \alpha_0, \alpha_\mrm{exp},
      \beta_0, \beta_\mrm{exp}, \epsilon_0, \epsilon_C, \epsilon_\mrm{min}}
    && \text{FAQL, $\epsilon$-greedy} \\
    &\set{\gamma, \alpha_0, \alpha_\mrm{exp},
      \beta_0, \beta_\mrm{exp},\tau_0, \tau_C, \tau_\mrm{min}}
    && \text{FAQL, softmax}
    .
  \end{aligned}\label{eq:hyperspace}
\end{equation}
The values for the hyper-parameters were chosen by random grid search
\cite{bergstra_random_2012}: first, some random points were drawn
uniformly from a large rectangle in the parameter space and used to
train the algorithm, then the search region was narrowed following the
distribution of the best found points and the process was iterated
until the average reward from the best points did not increase
anymore.
%
The high computational cost of testing each point (\num{2000000}
training iterations and \num{10000} test iterations, averaged over 10
runs) makes this a better choice than regular grid search, because it
avoids the possibility of wasting many train-test cycles in the
sampling of a single parameter, that may be later found to not affect
the algorithm performance.

The best hyper-parameters that were found for each one of the three
algorithms are shown in Table~\ref{tab:single_ql_params}, where
Q-Learning performed best coupled with a softmax exploration policy,
while FAQL and RUQL worked better with an $\epsilon$-greedy policy.
%
The values of $\epsilon_\mrm{min}$ and $\tau_\mrm{min}$ were fixed to
\num{1e-3} and the range for $\epsilon_C$ and $\tau_C$ was set in
order to keep both parameters above their respective minimum until the
last training step.
%
\begin{table}[htbp]
  \centering
  \begin{tabular}{*{4}{C}}
    \toprule
                     & \text{QL} & \text{FAQL} & \text{RUQL} \\
    \midrule
    \gamma           & 0.603     & 0.760       & 0.630       \\
    \alpha_0         & 0.718     & 0.886       & 0.557       \\
    \alpha_\mrm{exp} & 0.506     & 0.543       & 0.510       \\
    \beta_0          & -         & 0.506       & -           \\
    \beta_\mrm{exp}  & -         & 0.279       & -           \\
    \epsilon_0       & -         & 0.0919       & 0.400       \\
    \epsilon_C       & -         & 2.044\cdot10^{-8} & 7.945\cdot10^{-8} \\
    \epsilon_\mrm{min} & -       & 10^{-3}     & 10^{-3}     \\
    \tau_0           & 0.0590    & -       & -           \\
    \tau_C           & 1.536\cdot10^{-8} & - & - \\
    \tau_\mrm{min}   & 10^{-3}   & -           & - \\
    \bottomrule
  \end{tabular}
  \caption{Hyper-parameters for the three Q-Learning-based algorithms.
    They were selected by manually-tuned random grid search,
    except for $\epsilon_\mrm{min}$ and $\tau_\mrm{min}$.}
  \label{tab:single_ql_params}
\end{table}
%
The table shows a difference between the kind of points that worked
best for the various algorithms. RUQL seems to perform better when
starting with a policy that is very exploratory at first and becomes
greedier sooner than the policies of FAQL and QL. FAQL and QL, on the
other hand, seem to work best when they start with a policy that is
already quite greedy and that changes very slowly.  The difference in
the choice of parameters between QL and FAQL is that the learning rate
of the latter decays faster.

\subsection{Performance comparison}
After the hyper-parameter selection, each algorithm from the previous
section was tested to show its learning speed.  The testing process
consisted in extracting the value function estimate from an agent
being trained over \num{2000000} steps, at regular intervals
($\setst{Q_k(s,a)}{k = \frac{1}{24}K,\frac{2}{24}K,\dots,K,\; K =
  2\cdot10^6}$), constructing a greedy policy from each estimate
($\pi^{(k)}(s,a) = \ind{a = \argmax_a Q_k(s,a)}$) and running it over
a new \num{10000}-step episode without learning.
%
The metric used to evaluate the agent during the tests was the average
reward per step obtained during the episode:
\begin{equation}
  \frac{1}{J} \sum_{j=0}^{J-1} R(s(j), \argmax_a Q_k(s(j),a), s(j+1)),
  \quad J=\num{10000}
  .
\end{equation}

The results of this testing process, averaged over 200 training runs,
are shown in Figure~\ref{fig:single_avg_reward}.
%
\begin{figure}[htbp]
  \centering
  \begin{subfigure}{\subfigwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/single/avg_reward_ql}
    \caption{Q-Learning}
  \end{subfigure}
  \begin{subfigure}{\subfigwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/single/avg_reward_faql}
    \caption{FAQL}
  \end{subfigure}
  \begin{subfigure}{\subfigwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/single/avg_reward_ruql}
    \caption{RUQL}
  \end{subfigure}
  \caption{Training results. The agents are tested over 24 episodes of
    \num{10000} steps, spread regularly over the \num{2000000}
    training steps. The plots show the average and its
    \SI{95}{\percent} CI of 200 training runs. They are compared with
    the results obtained by uniform action selection (RW) and 3-step
    lookahead (LA) averaged over 50 episodes. }
  \label{fig:single_avg_reward}
\end{figure}
%
It can be seen from the plots that all three algorithms improve the
policy over the course of the training and reach an average reward per
slot close to \num{0.5}. The best performing algorithm is RUQL, since
its average reward per step reaches 0.524, compared to 0.491 and 0.483
reached by QL and FAQL respectively.
%
The average reward gained by QL and FAQL is essentially the same in
the long run, since the confidence intervals overlap after
approximately 1.2 million training steps.
%
Compared to standard Q-Learning, FAQL behaves more randomly at the
beginning of the training process, as can be seen from the confidence
intervals, but converges a bit sooner than QL: 1.16 million steps
instead of 1.5 million steps.

In terms of the average reward per step, RUQL is clearly the best
algorithm among the three considered here, since it gives a higher
reward with a lower variance.  The most important difference between RUQL
and either FAQL or QL is, however, its speed of convergence: the
faster of the two still needs approximately 1.16 million steps of
training to reach convergence, while RUQL already converges after
approximately \num{600000} steps.
%
Here, the dependence of the learning rate on the exploration policy
allows the agent to adjust its value function estimates faster than
standard QL and still converge to a good policy. As expected from
\cite{abdallah_addressing_2016}, this dynamic learning rate
allowed RUQL to outperform QL over a non-stationary environment.

In Figure~\ref{fig:single_test_rewards}  the average rewards
obtained by the fully trained agents (after \num{2000000} steps) over
the course of a test episode are shown.
%
\begin{figure}[htbp]
  \centering
  \includegraphics[width=\figwidth]{figures/single/test_rewards}
  \caption{Rewards obtained by the fully trained agents over a
    \num{10000} steps episode.}
  \label{fig:single_test_rewards}
\end{figure}

The slightly higher complexity of the FAQL and RUQL algorithms, with
respect to standard Q-Learning, translates to a slower execution time;
Figure~\ref{fig:single_train_times} shows that this is, indeed, true.
The box plots compare the execution times of a single training step
for the three algorithms, which were recorded over 50 training
episodes of \num{10000} steps each.
%
As can be seen from the box plots, RUQL seems to be faster than FAQL.
This is likely to be due to implementation details, however, since the
update rules used by the two algorithms differ only in the computation
of the effective learning rates (\ref{eq:faql_effective_alpha} and
\ref{eq:ruql_effective_alpha}) and both could be further optimized, if
needed.
%
All three algorithms, in any case, required less than \SI{2.1}{\ms}
for \SI{99}{\percent} of the training steps considered in this
chapter. The total training time for \num{ 2000000} steps that these
results imply (approximately \SI{1.2}{\hour}) is consistent with the
time taken to run the simulations.
%
\begin{figure}[htbp]
  \centering
  \includegraphics[width=\figwidth]{figures/single/train_times}
  \caption[Single-agent execution time]{Distribution of the execution
    time for a single training step, averaged over 50 episodes of
    \num{10000} steps each.  The box plot whiskers are set to show the
    \nth{1} and \nth{99} percentiles and the outliers are omitted. The
    simulations ran on a small cluster of AWS~C5\footnotemark\ virtual
    machines.}%
  \label{fig:single_train_times}
\end{figure}
\footnotetext{see~\url{https://aws.amazon.com/ec2/instance-types/}}

Figure~\ref{fig:single_test_times} compares, instead, the execution
times of a single test step for the same three algorithms together
with the three-step greedy lookahead planner. Like in the previous case,
the timing data was collected over 50 episodes of \num{10000} steps,
but the three QL-based algorithms were also trained for \num{100000}
steps before each episode.
%
The execution times for the three QL-based algorithms were very
similar: the medians for Q-Learning, FAQL and RUQL are, respectively,
\SI{0.742}{\ms}, \SI{0.762}{\ms} and \SI{0.756}{\ms}.  This similarity
was expected because, after the training has finished, the agents that
employ these algorithms just need to compute an argmax over, at most,
$\abs{\mathcal{A}}$ values.
%
Testing the three algorithms for a single step over the POMDP
described in this chapter required less than \SI{1.072}{\ms} in
\SI{99}{\percent} of the cases.
%
\begin{figure}[htbp]
  \centering
  \includegraphics[width=\figwidth]{figures/single/test_times_log}
  \caption{Distribution of the execution time for a single test step,
    averaged over 50 episodes of \num{10000} steps each. The three
    QL-based algorithms are trained for \num{100000} steps before each
    episode.  The box plot whiskers are set to show the \nth{1} and
    \nth{99} percentiles and the outliers are omitted. The simulations
    ran on a small cluster of AWS~C5 virtual machines.}
  \label{fig:single_test_times}
\end{figure}
%
For comparison, the statistics of the time to execute a random action
are also shown; which essentially represents the time required to
simulate a step of the \ac{MDP}.
%
%% As can be seen, the trained agents require a
%% computation time to choose their next action that is comparable to the
%% time that is required to just simulate the underlying MDP, without
%% spending any effort on the action choice.
%
The box plots also show that the lookahead planning is more
time-consuming by two orders of magnitude, since it requires a median
of \SI{227}{\ms} for each step, which is almost 300~times the
execution time of a RUQL step.

The above results show that the three QL-based algorithms considered
in this thesis can be used to train a single UAV to patrol a cell grid
in order to obtain a performance level close to that given by greedy
lookahead planning.
%
The RUQL algorithm performs better than the other two both in terms of
average reward gained by the trained agent and in terms of convergence
speed. Its only disadvantage is a slightly higher training time per step
with respect to standard QL.
%
The lower average reward with respect to greedy lookahead is
compensated by the advantages enumerated below.
\begin{itemize}
  \item First of all, in order to apply the lookahead algorithm the
    transition function must be known, while none of the QL-based
    algorithms need such information.
  \item Also, while the lookahead algorithm can, in principle, be used
    to compute a policy offline, doing so requires the planning to be
    repeated for every reachable state-action pair. The QL-based
    algorithms, instead, tend to prefer the most promising states
    during the training.
  \item
    Once the agents are trained, their policy can also be executed 300
    times faster compared with the lookahead policy.
  \item Moreover, if the transition function were to change during the
    patrolling, it would be possible to adapt FAQL or RUQL to work
    online.  The lookahead approach, instead, never makes exploratory
    actions and cannot adapt to a new transition function, unless such
    function is given to it.
 \end{itemize}

%  \section{Results for a reward process with randomized parameters}
% \todo[inline]{Either: single random Pon, Poff then same training process,
% or: new random Pon, Poff for each training run.}
