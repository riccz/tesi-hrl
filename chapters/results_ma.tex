% !TEX root = ../tesi.tex

\chapter{Multi-agent results}\label{ch:results_ma}
In this chapter the multiple-MDP model from Chapter~\ref{ch:model}
will be simplified in a way similar to Chapter~\ref{ch:results_sa},
adapting it to the case where multiple agents must coexist.
%
Then the results of the simulations with four agents will be
discussed.
%
The multi-agent problem will be solved like in
Chapter~\ref{ch:results_sa}, replacing the $n$-step lookahead with an
MCTS algorithm for computational complexity reasons.

The number of agents considered in this chapter is $\Nag = 4$, while
leaving the rest of the system unchanged with respect to
Chapter~\ref{ch:results_sa}.

\section{Multi-agent POMDP}\label{ma:pomdp}
As already mentioned in Section~\ref{mod:mdp}, now that there is more
than one agent acting simultaneously, they are considered separately
as $\Nag$ MDPs that share a global state. The actual (Markovian)
transition function determines the global state evolution together
with the joint actions of the agents.
%
Each agent, however, independently controls its own actions, receives
its own reward and interacts with the other agents only through the
shared global state.

In this case, stationariety is lost as soon as the agents are learning
independently and, moreover, the same concerns raised in
Chapter~\ref{ch:results_sa} regarding the continuity and the size of
the state space apply here.  The size of the state space is now even
more of a problem because each additional agent needs its own state
variables.

Regarding the battery's and the cell grid's state, the same approach
of Chapter~\ref{ch:results_sa} is employed here, with the addition
that each agent ignores the battery state of every other agent and the
cell states that they see.

While it is reasonable to ignore the battery state of other agents,
knowing their position in the grid should help the agents to spread
more uniformly over the grid.
%
In order to avoid having an observation space that grows exponentially
in $\Nag$, only the position of the closest agent is put into the
observations. This position is represented using the distance $d_i(k)$
and the angle $\theta_i(k)$ which, being continuous variables, must be
quantized to fit in a finite observation space.  The angle is
quantized in four uniform steps, aligned with the movement directions:
\begin{equation}
  \tilde{\theta}_i(k) \in \tilde{\Theta} =
  \set{-\frac{\pi}{2}, 0, \frac{\pi}{2}, \pi}
\end{equation}
and the distance is quantized non-uniformly with four geometrically
increasing intervals, like the battery (see Section~\ref{sa:pomdp});
the upper bound of the first interval is set just below $L$, in order
to distinguish overlapping agents from agents one step away, while the
upper bound of the last interval is set to the diameter of the grid:
\begin{equation}
\tilde{d}_i(k) \in \tilde{\mathcal{D}} = \set{0.500L, 1.46L, 2.80L, 5.38L}
.
\end{equation}
If two agents overlap, the angle between them is set to zero.  When
there is only a single agent on the grid, that agent will have the
angle set to zero and the distance set to infinity.

Using these two additional variables to represent the position of the
closet agent, each one of the agents must solve the POMDP
$\left(\mathcal{S}, \Omega, \mathcal{A}, T_i, O_i, R_i\right)$ where
$\mathcal{S}$, $\mathcal{A}$, $T_i$ and $R_i$ are the same state
space, action space, transition function and reward function from
Chapter~\ref{ch:model}.  The observation space can be defined,
similarly to Chapter~\ref{ch:results_sa} as:
\begin{equation}
  \begin{aligned}
    \Omega=&\left(\set{\OFF, \ON}\times\set{(x, y, \tilde{b}, a^{(-1)},
      \tilde{d}, \tilde{\theta})} \right)
    \\ &\cup \set{0, 1, \dots, N_\mrm{ret}-1},  \\
    & x \in \mathcal{X}, y \in \mathcal{Y}, \tilde{b} \in \tilde{\mathcal{B}},
    a^{(-1)} \in \mathcal{A},
    \tilde{d} \in \tilde{\mathcal{D}},
    \tilde{\theta} \in \tilde{\Theta} ,
  \end{aligned}
  \label{eq:multi_obs_space}
\end{equation}
and it is the same for every agent.
%
The corresponding observation function is still deterministic and has
the same shape for each agent:
\begin{equation}
  O_i(s', a_i, o_i) = \begin{cases}
    \ind{ o_i = s_i' } &\text{if}\; s_i' \in \mathcal{S}_\mrm{ret} \\
    \ind{ o_i = (z_{x_i',y_i'}, x_i', y_i', \tilde{b}_i', a_i,
      \tilde{d}_i', \tilde{\theta}_i') }
    &\text{otherwise}
  \end{cases}
  .
\end{equation}
Once the closest agent is denoted by $j = \argmin_j L\sqrt{{(x_i' -
  x_j')}^2 + {(y_i' - y_j')}^2}$, the three quantized variables above are
\begin{align}
  \tilde{b}_i' &= \argmin_{\tilde{B}_l}\abs{\tilde{B}_l - b_i} \\
  \tilde{d}_i' &=
  \begin{cases}
    \argmin_{\tilde{D}_l}\abs{\tilde{D}_l -
      L\sqrt{{(x_i' - x_j')}^2 + {(y_i' - y_j')}^2}} & \text{if}\; \exists j \\
    \infty & \text{otherwise}
  \end{cases} \\
  \tilde{\theta}_i' &= \begin{cases}
    0 & \text{if}\; \tilde{d}_i' = \tilde{D}_0, \infty \\
    \argmin_{\tilde{\Theta}_l} \abs{\tilde{\Theta}_l -
    \arctan\frac{y_j' - y_i'}{x_j' - x_i'} + \pi\ind{x_j' < x_i'}}
    &\text{otherwise}
  \end{cases}
\end{align}

In order to compare the performance given by the algorithms considered
here in the multi-agent case, with respect to
Chapter~\ref{ch:results_sa}, it is useful to define a single metric.
%
The metric used to evaluate the multi-agent system performance in the
following paragraphs will be the global average reward per step
obtained during a \num{10000}-step episode:
\begin{equation}
  \frac{1}{J} \sum_{j=0}^{J-1}
  \frac{1}{\Nag} \sum_{i=0}^\Nag R_i(s(j), a_i(j), s(j+1)),
  \quad J=\num{10000}
  .
\end{equation}
%
This metric is used only for the purposes of optimizing the
hyper-parameters and assessing the global system performance; the
agents themselves are still optimizing their own reward $R_i$.

\todo{ Using this average reward per agent, this setup is very similar
  to the framework of \emph{Decentralized} POMDP described by
  \cite{wiering_reinforcement_2012} (specific chap) (with a continuous
  instead of finite state). The reward function is the only diff.}

The upper bound \eqref{eq:single_bound} from
Chapter~\ref{ch:results_sa} still applies, although its assumptions are
becoming too optimistic. With multiple agents collecting rewards
simultaneously it is, indeed, more difficult to be always able to
enter an ON cell. The assumption of a full reset of the grid at each
battery recharge is also much more optimistic when multiple agents are
present.

\todo{Semi-Markov model?  no battery but track the number of
  available ON states. With one agent gives a trivial bound because
  $\pi_0 \approx 0$}

\section{Monte Carlo Tree Search}
Unlike in Chapter~\ref{ch:results_sa}, in the four-agent case it is
not feasible to apply the greedy lookahead algorithm. The computation
of the observation probabilities would still require a full
enumeration of the possible states, whose number scales exponentially
in $\Nag$.
%
To avoid this problem, the lookahead algorithm will be replaced with
an MCTS algorithm (see Section~\ref{reinf:mcts}) which allows the
agents to plan without directly using the transition function and
using, instead, a generative model.

Since each agent only plans its own actions, it does not know the
policy followed by the other agents. One possible strategy would be to
estimate the other agents'~policies and include them in the transition
probabilities. However, this would increase the computational load and
would require a bigger observation space to take into account the
other agents'~state-action pairs. For this reason, each agent plans
assuming that the other agents adopt a uniform random policy. This
should not affect the performance too much, because the planning is
repeated at each new action starting from the actual state.

The specific MCTS algorithm that was applied to the multi-agent
problem is the Sparse UCT algorithm described in
Section~\ref{reinf:uct}), which is a variation
by~\cite{bjarnason_lower_2009} on the UCT algorithm described
by~\cite{hutchison_bandit_2006}. The modification consists of capping
the number of children of an action node. In this thesis it was capped
to $w=16$.
%

The reason for the cap on the number of children given by the authors
of Sparse UCT also applies here. Moreover, the huge amount of possible
next states in the MDP considered here would reduce this algorithm to
standard Monte Carlo rollout from the root state. This would happen
because very few states would be visited enough to actually explore
all actions, hence the tree policy would almost never be used.

The terminal states for UCT are set to be those at depth 6 and the
discount rate is set to $\gamma = 0.6$. The tail of the discounted
return is still non-negligible ($\sum_{k=7}^\infty \gamma^k \approx
0.07$) but this depth was chosen to keep the time needed to execute
the algorithm under reasonable limits.
%
%% \todo{ With max depth = 3, $\gamma=0.6$ the tail of the disc.gain is
%%   still high.  For tail $\leq 0.01$: 10 steps. For tail $\leq 0.001$:
%%   15 steps.  }
%
The exploration parameter in the action selection
(see~\eqref{eq:uct_action}) is set to $c = \sqrt{2}$, following
\cite{auer_finite-time_2002}.

Figure~\ref{fig:multi_avg_reward_all} shows the global average reward
obtained by Sparse UCT over 50 episodes of \num{10000} steps.  The
resource constraint was the number of iterations,set to 200.  As can
be seen the results are much lower than the optimistic bound,
suggesting that it may be possible to find a tighter bound.  The
global average reward was \num{0.427} with a standard deviation of
\num{4.96e-3}.  In Figure~\ref{fig:multi_test_rewards_MCTS} it is
possible to see the average reward obtained by each agent over the
course of one \num{10000}-step episode; all four agents obtain
average rewards close between them.

%% FAQL final avg.rew. = 0.405901, std.dev. = 0.005762
%% MCTS final avg.rew. = 0.427373, std.dev. = 0.004964
%% QL final avg.rew. = 0.412553, std.dev. = 0.005627
%% RUQL final avg.rew. = 0.418076, std.dev. = 0.005558
%% RW final avg.rew. = 0.162847, std.dev. = 0.002410

\section{Result comparison}
Since the three QL-based algorithms are applied unmodified to the
multi-agent case, all the same considerations from
Chapter~\ref{ch:results_sa} apply here as well.

The shapes of the learning rates and exploration parameters are still
those given by
(\ref{eq:ql_alpha},~\ref{eq:faql_beta},~\ref{eq:epsilon}
and~\ref{eq:tau}); hence, the hyper-parameter space for the three
algorithms is the same as in \eqref{eq:hyperspace}.

The parameters of Table~\ref{tab:multi_ql_params} were found by
following the same approach used in the single agent case (random grid
search tuned by hand).
\begin{table}[htbp]
    \centering
    \begin{tabular}{*{4}{C}}
      \toprule
                       & \text{QL} & \text{FAQL} & \text{RUQL} \\
      \midrule
      \gamma           & 0.436     & 0.437       & 0.724       \\
      \alpha_0         & 0.970     & 0.565       & 0.689       \\
      \alpha_\mrm{exp} & 0.530     & 0.704       & 0.511       \\
      \beta_0          & -         & 0.061       & -           \\
      \beta_\mrm{exp}  & -         & 0.004       & -           \\
      \epsilon_0       & -         & -           & -           \\
      \epsilon_C       & -         & -           & -           \\
      \epsilon_\mrm{min} & -       & -           & -           \\
      \tau_0           & 1.271     & 1.210       & 0.755           \\
      \tau_C           & 3.099\cdot10^{-8} & 4.443\cdot10^{-7} & 2.531\cdot10^{-7} \\
      \tau_\mrm{min}   & 10^{-3}   & 10^{-3}           & 10^{-3}   \\
      \bottomrule
    \end{tabular}
    \caption{Hyper-parameters for the three Q-Learning-based algorithms.
      They were selected by manually-tuned random grid search,
      except for $\epsilon_\mrm{min}$ and $\tau_\mrm{min}$.}
    \label{tab:multi_ql_params}
  \end{table}
This time, all three algorithms worked better together with a softmax
exploration policy. The decrease rate of the learning rates is similar
to the single-agent case: RUQL performs better with a smaller,
slowly-decreasing $\alpha$, while the other two algorithms have a
better performance with a faster-decaying learning rate that starts
from a higher initial value. It can be noticed that $\beta$ is very
small during all the training process. The exploration parameters seem
to influence the performance differently this time: RUQL has, unlike
in the single agent case, the greediest policy of the three; for FAQL
and QL, like in the single-agent case, the first tends to become
greedy slightly sooner.

Figures~\ref{fig:multi_avg_reward} and~\ref{fig:multi_avg_reward_all}
show the global average reward obtained as the number of training
steps increases. The testing process is the same as in
Chapter~\ref{ch:results_sa}: the agents are trained for a total of
\num{2000000} steps, at 24 points regularly spread over the training
steps the value function estimate is extracted and tested over a
\num{10000}-step episode; the training process is averaged over 200
runs.

Like for the single agent, RUQL (average reward 0.418) performs
slightly better than Q-Learning (average reward 0.413), which performs
slightly better than FAQL (average reward 0.406) in terms of average
reward at convergence. This time, however, the three algorithms behave
almost identically: they all reach convergence after approximately 1.5
million steps, with a very slow increase rate starting from
approximately \num{750000} steps; FAQL does not exhibit a high initial
variance anymore.
%
\begin{figure}[htbp]
  \centering
  \begin{subfigure}{\subfigwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/multi/avg_reward_ql}
    \caption{Q-Learning}
  \end{subfigure}
  \begin{subfigure}{\subfigwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/multi/avg_reward_faql}
    \caption{FAQL}
  \end{subfigure}
  \begin{subfigure}{\subfigwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/multi/avg_reward_ruql}
    \caption{RUQL}
  \end{subfigure}
  \caption{Training results. The agents are tested over 24 episodes of
    \num{10000} steps, spread regularly over the \num{2000000}
    training steps. The plots show the average and its
    \SI{95}{\percent} CI of 200 training runs. They are compared with
    the results obtained by uniform action selection (RW) and Monte
    Carlo Tree Search (MCTS) averaged over 50 episodes. }%
  \label{fig:multi_avg_reward}
\end{figure}
%
\begin{figure}[htbp]
    \centering
    \includegraphics[width=\figwidth]{figures/multi/avg_reward_all}
    \caption{Zoomed training results from
      Figure~\ref{fig:multi_avg_reward}. The agents are tested over 24
      episodes of \num{10000} steps, spread regularly over the
      \num{2000000} training steps. The plots show the average and its
      \SI{95}{\percent} CI of 200 training runs. They are compared
      with the results obtained by MCTS averaged over 50 episodes.}%
    \label{fig:multi_avg_reward_all}
\end{figure}

Figure~\ref{fig:multi_test_rewards} shows separately the rewards
gained by the four fully trained agents over the course of a
\num{10000} steps episode. This shows that they all learn in a similar
way.
\begin{figure}[htbp]
  \centering
  \begin{subfigure}{\subfigwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/multi/test_rewards_RW}
    \caption{RW}
  \end{subfigure}
  \begin{subfigure}{\subfigwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/multi/test_rewards_MCTS}
    \caption{MCTS}
    \label{fig:multi_test_rewards_MCTS}
  \end{subfigure}
  \begin{subfigure}{\subfigwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/multi/test_rewards_QL}
    \caption{QL}
  \end{subfigure}
  \begin{subfigure}{\subfigwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/multi/test_rewards_FAQL}
    \caption{FAQL}
  \end{subfigure}
  \begin{subfigure}{\subfigwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/multi/test_rewards_RUQL}
    \caption{RUQL}
  \end{subfigure}
  \caption{Rewards obtained by the fully trained agents over a
    \num{10000} steps episode. The reward for each agent is shown
    separately here.}
  \label{fig:multi_test_rewards}
\end{figure}

The time required to execute a training step was measured over 50
episodes of \num{10000} steps, using the three QL-based algorithms.
With multiple agents, the training time is considered to be the time
required to perform one update step \emph{for all agents}.
%
Like for the single-agent case, RUQL (median \SI{5.49}{\ms}) and FAQL
(median \SI{5.50}{\ms}) are slightly more computationally demanding
than standard Q-Learning (median \SI{5.00}{\ms}). Now the difference
between FAQL and RUQL, which was visible in the single-agent case, is
much smaller. It can also be noted that the independent training times
are significantly less than four times the correspondent ones in the
single-agent case.
%
\begin{figure}[htbp]
  \centering
  \includegraphics[width=\figwidth]{figures/multi/train_times}
  \caption{Distribution of the execution time for a single training
    step, averaged over 50 episodes of \num{10000} steps each.  The
    box plot whiskers are set to show the \nth{1} and \nth{99}
    percentiles and the outliers are omitted. The simulations ran on a
    small cluster of AWS~C5 virtual machines.}
  \label{fig:multi_train_times}
\end{figure}

Figure~\ref{fig:multi_test_times}, instead, shows the time required to
apply the learned policies and compares it to the time required to
plan using Sparse UCT. The time is averaged across 50 episodes of
\num{10000} steps; the three QL-based algorithms were additionally
trained for\num{100000} steps prior to the testing process.
%
\begin{figure}[htbp]
  \centering
  \includegraphics[width=\figwidth]{figures/multi/test_times_log}
  \caption{Distribution of the execution time for a single test step,
    averaged over 50 episodes of \num{10000} steps each. The three
    QL-based algorithms are trained for \num{100000} steps before each
    episode.  The box plot whiskers are set to show the \nth{1} and
    \nth{99} percentiles and the outliers are omitted. The simulations
    ran on a small cluster of AWS~C5 virtual machines.}
  \label{fig:multi_test_times}
\end{figure}
%
Like in the single-agent case, the time to execute a step using the
three QL-based algorithms is almost identical. For all three
algorithms it is below \SI{3.90}{\ms} for more than \SI{99}{\percent}
of the time samples.  Similarly to the training times, the test times
are less than four times the corresponding single-agent test times;
this time, however, they are quite close.  The increase in the random
walk time can also be noticed, which reflects the increased complexity
of the system.
%
The time to plan a single action using Sparse UCT (median
\SI{8.16}{\s}) is much higher even than the one that was required for
greedy lookahead on the single-agent case. Compared to the QL-based
algorithms, Sparse UCT requires a time larger by three orders of
magnitude (more than \num{2700} times).

%% Even considering only some
%% of the paths that start from the agent's state and limiting the number
%% of states considered after each state-action pair, the time required
%% to plan an action is still approximates XX times the one of the
%% QL-based algorithms.


%% TRAIN
%% QL mean = 4.859517 [ms]
%% FAQL mean = 5.399496 [ms]
%% RUQL mean = 5.352444 [ms]
%% QL median = 5.000795 [ms]
%% FAQL median = 5.496741 [ms]
%% RUQL median = 5.488349 [ms]
%% QL whisk0 = 4.613988 [ms]
%% QL whisk1 = 3.176838 [ms]
%% QL whisk0 = 5.080147 [ms]
%% QL whisk1 = 6.068948 [ms]
%% FAQL whisk0 = 5.119420 [ms]
%% FAQL whisk1 = 3.873473 [ms]
%% FAQL whisk0 = 5.584339 [ms]
%% FAQL whisk1 = 6.665510 [ms]
%% RUQL whisk0 = 5.106005 [ms]
%% RUQL whisk1 = 3.466948 [ms]
%% RUQL whisk0 = 5.579727 [ms]
%% RUQL whisk1 = 6.569736 [ms]

%% TEST
%% RW mean = 2.013520 [ms]
%% MCTS mean = 8104.328311 [ms]
%% QL mean = 3.093498 [ms]
%% FAQL mean = 3.035440 [ms]
%% RUQL mean = 3.008538 [ms]
%% RW median = 2.098016 [ms]
%% MCTS median = 8160.245598 [ms]
%% QL median = 3.159290 [ms]
%% FAQL median = 3.136164 [ms]
%% RUQL median = 3.123487 [ms]
%% RW whisk0 = 2.015465 [ms]
%% RW whisk1 = 1.152013 [ms]
%% RW whisk0 = 2.156120 [ms]
%% RW whisk1 = 2.318821 [ms]
%% MCTS whisk0 = 7912.610619 [ms]
%% MCTS whisk1 = 7140.870934 [ms]
%% MCTS whisk0 = 8384.268698 [ms]
%% MCTS whisk1 = 8578.859997 [ms]
%% QL whisk0 = 3.089846 [ms]
%% QL whisk1 = 1.825201 [ms]
%% QL whisk0 = 3.218946 [ms]
%% QL whisk1 = 3.899100 [ms]
%% FAQL whisk0 = 3.076766 [ms]
%% FAQL whisk1 = 1.796685 [ms]
%% FAQL whisk0 = 3.186456 [ms]
%% FAQL whisk1 = 3.626347 [ms]
%% RUQL whisk0 = 3.046562 [ms]
%% RUQL whisk1 = 1.764936 [ms]
%% RUQL whisk0 = 3.181370 [ms]
%% RUQL whisk1 = 3.813467 [ms]

The above results show that the three QL-based algorithms considered
in this thesis can also be applied successfully to the multi-agent
case. They all reach the same performance, in terms of average
reward, convergence speed and execution times.  Their average reward
is very close to the one obtained by an MCTS algorithm, but they have
some advantages over it. The QL-based algorithms can learn purely from
the experience obtained along some trajectories in the MDP, while MCTS
needs to be able to simulate transitions starting from arbitrary
states. This is a less restrictive requirement than knowing the full
transition function, but still makes MCTS more difficult to apply in
unknown environments.
%
Another advantage of QL-based algorithms over MCTS is the small time
needed to apply the learned policies. This is balanced by the fact that
they require to be trained, but if the agents are expected to be
running for a long time, the shorter policy execution time is more
important.
