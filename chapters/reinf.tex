\chapter{Basics of reinforcement learning}\label{ch:reinf}
\ac{RL} is a branch of machine learning that deals with problems that
require an agent to control the state of the environment by applying a
control action, while receiving a feedback in the form of a numeric
reward. This formulation of the reinforcement learning problem can
also be seen \cite{sutton_reinforcement_2018} as the approximation of
the optimal control of a Markov Decision Process, which will be
defined precisely in Section~\ref{reinf:mdp}.

This kind of learning is very different from the most popular machine
learning paradigms \cite{sutton_reinforcement_2018,
  wiering_book_chap1}: \emph{supervised} learning and
\emph{unsupervised} learning.
%
In the supervised learning framework there is a training set available
where the samples are labeled with the \emph{correct} (or
\emph{optimal}) answer that the learning algorithm should give. In RL,
instead, there is no such kind of data available: the agent never
knows if the actions it picks are the optimal ones, it can only infer
the ``goodness'' of such actions by observing the rewards that are
collected during the interaction with the environment.
%
Reinforcement learning also has a different objective than
unsupervised learning, even if both frameworks do not have access to
labeled samples: while unsupervised learning aims to learn the
structure (i.e.\ the probability distribution) of the data it is given,
RL does not need to estimate an explicit model of the environment to
choose optimal actions. Indeed, there are many \emph{model-free} RL
algorithms, like Q-learning (see Section~\ref{reinf:qlearn}) and
some of its variants (see Section~\ref{reinf:qlearn_ext}), which
learn optimal actions without explicitly considering a model of the
environment.
%
Another difference between supervised/unsupervised learning and RL is
the way in which the data samples are acquired. Typically, in
supervised and unsupervised learning the samples are drawn
independently from a static probability distribution. In \ac{RL},
instead, the samples are often not independent, because they are taken
along the path followed by the agent in the state space, and they may
also come from a non-stationary distribution. This is the case, for
instance, when the policy used to pick the actions is refined during
the exploration of the environment, without waiting for the end of the
episode to use the observed samples.

An extensive description of the reinforcement learning basics is given
by~\cite{sutton_reinforcement_2018, wiering_reinforcement_2012}.

In the following sections the reinforcement learning problem is
defined precisely, along with a well-known algorithm that can, in
principle, solve it. Then the Q-learning algorithm is explained,
together with two related extensions that aim to improve some
shortcomings of the original version. A planning algorithm that will
be needed in the multi-agent case is also introduced and, finally, the
extension of the \ac{MDP} framework to the multi-agent case is
discussed.

\section{\aclp*{MDP}}\label{reinf:mdp}
As mentioned at the beginning of Chapter~\ref{ch:reinf}, the basic
reinforcement learning framework (as described in
\cite{sutton_reinforcement_2018}) consists of an agent that, at each
decision time $t$:
\begin{itemize}
\item observes the state $s_t$ of the surrounding environment, which
  belongs to a set $\mathcal{S}$ of possible states;
  \item chooses an action $a_t$ from the set $\mathcal{A}(s_t)$ of
    available actions in state $s_t$;
  \item observes the next state of the environment $s_{t+1} \in
    \mathcal{S}$, that can depend on the action chosen by the agent;
  \item receives a reward $r_{t+1} \in \realset$, which is an
    indication of the ``goodness'' of the agent's behavior.
\end{itemize}
%
If the stochastic process that generates the pair $(s_{t+1}, r_{t+1})$
enjoys the Markov property, then this decision problem is \iac{MDP}.

Following~\cite{wiering_book_continuous}, a continuous-state
finite-action \ac{MDP} is a tuple $(\mathcal{S}, \mathcal{A}, T, r)$
where $\mathcal{S}$ is a measurable (with a $\sigma$-algebra
$\Sigma_\mathcal{S}$) space of possible states,\todo{actually in the
  book $S \subset \realset^D$, but here there are discrete dims:
  count.meas.} $\mathcal{A}$ is a finite set of actions that the agent
can execute, $T$ is a conditional \ac{PDF} for the next state and $r$
is a deterministic function that gives the agent's reward.
%
If the set of available actions is different at each state, they can
be denoted by the function $\mathcal{A}(\cdot): \mathcal{S} \to
\powerset{\mathcal{A}}$, considering $\mathcal{A}$ as the superset of
all possible action sets.
%
The \ac{PDF} for the next state is called \emph{transition function}
$T: \mathcal{S} \times \mathcal{A} \times \mathcal{S} \to [0,1]$ and
defines a conditional probability distribution over $\mathcal{S}$ such
that
\begin{equation}
  \begin{aligned}
  &\int_{S'}T(s,a,s')\mrm{d}s' = \ProbC{s_{t+1} \in S'}{s_t = s, a_t = a}
  \\ &\forall s \in \mathcal{S}, a \in \mathcal{A}(s),
  S' \in \Sigma_\mathcal{S}
  \end{aligned}
  .
\end{equation}
%
The \emph{reward function} is $r: \mathcal{S} \times \mathcal{A}
\times \mathcal{S} \to \realset$ and gives the reward $r(s,a,s')$
obtained during a transition from $s$ to $s'$ with action $a$.

In general, the transition function and the reward function can also
be time-dependent, but many \ac{RL} algorithms assume that they do
not. In this latter case the \ac{MDP} is said to be \emph{stationary}.

If, in addition to the action space, the state space is also finite,
the MDP, too, is said to be \emph{finite}. In this case the
definitions remain the same, but it is more convenient to see the
transition function as
\begin{equation}
  T(s,a,s') = \ProbC{s_{t+1} = s'}{s_t = s, a_t = a}
  \quad \forall s,s' \in \mathcal{S}, a \in \mathcal{A}(s)
  .
\end{equation}

The most common definitions of \iac{MDP}, like those given
in~\cite{sutton_reinforcement_2018, wiering_book_chap1} deal only with
the case where both state and action spaces are finite and where the
state of the environment is sampled at discrete time steps (called
\emph{decision times}).
%
As will be seen in Chapter~\ref{ch:model}, however, the requirement of
a finite state space will make it difficult to model the system
considered in this thesis as \iac{MDP}. Hence, the more general
definition given above, where the state space is continuous, is the
one that will be used in this thesis.

The continuous-state MDP model will then have to be discretized in
order to apply the RL algorithms described below, but the model will
lose the Markov property (see Sections~\ref{sa:pomdp}
and~\ref{ma:pomdp}). It is therefore necessary to introduce
\emph{Partially Observable} Markov Decision Processes, in order to
model the discretization.

\section{Partially Observable Markov Decision Processes}\label{reinf:pomdp}
When the agent can not be assumed to know completely the state of the
environment, the MDP framework described above must be modified to
take into account that the agent can only see a (generally stochastic)
\emph{observation} of the state.

As described in~\cite{wiering_book_pomdp}, a \emph{Partially
  Observable} MDP is a tuple $(\mathcal{S}, \Omega, \mathcal{A}, T, O,
r)$ where the state space, action space, transition function and
reward function are exactly as in the standard MDP defined in
Section~\ref{reinf:mdp}.
%
The difference with respect to an MDP is that, when the agent performs
action $a$ in state $s$, it does not see the actual new state $s' \in
\mathcal{S}$, but the observation $o' \in \Omega$ with probability
$O(s',a,o')$. Thus the agent sees a sequence of transitions $(o, a, r,
o')$ between observations, instead of $(s,a,r, s')$.

From the point of view of the agent, the observations it receives can
be considered as states of a decision process themselves. However,
this decision process is not Markovian because the new observation
$O_{t+1}$ is not conditionally independent of past observations, given
$O_t$ and $A_t$.
%
This makes the treatment of general POMDPs much more complex that
MDPs, because now the agent cannot rely on just the last observation
to decide which action to take, but it must take into account the
entire observation-action history or, equivalently, a probability
distribution over the actual state derived from the history.

As will be seen later, in the POMDPs considered this thesis, the
observations retain enough information about the state to be used
directly as if they were actual states, with good results.

\section{Value functions}
When an agent has to choose an action in \iac{MDP}, it assigns a
probability to each possible action and randomly picks according to
this probability, which is called a \emph{policy}. It can be defined
as a time-dependent conditional probability distribution over the
action space:
\begin{equation}
  \pi_t(s, a) = \ProbC{A_t = a}{s_t = s} \quad s \in \mathcal{S}, a
  \in \mathcal{A}(s) .
\end{equation}
Deterministic policies, where only one action can be chosen with
non-zero probability are often denoted more compactly by $\pi_t(s)$,
which corresponds to
\begin{equation}
  \pi_t(s) = a \quad \text{if} \quad \ProbC{a_t = a}{s_t = s} = 1
  \quad s \in \mathcal{S}, a
  \in \mathcal{A}(s) .
\end{equation}

There exist multiple ways to measure the performance of a policy on
\iac{MDP}. A common one, which will be used throughout the rest of
this thesis, is the expected \emph{discounted
  return}~\cite{sutton_reinforcement_2018}
\begin{equation}
  \E{R_t} = \E{\sum_{k=0}^\infty \gamma^{t+k}r(s_{t+k}, a_{t+k}, s_{t+k+1})}  \label{eq:def_return}
\end{equation}
that takes into account the average reward that can be gained in the
future, while giving more importance to immediate rewards.
%
The \emph{discount rate} $\gamma \in [0, 1)$ represents the decreasing
importance given to future rewards.
%
Starting from the discounted return defined in~\eqref{eq:def_return}
it is possible to define the \emph{value} of a state under a given
policy as:
\begin{equation}
  V^\pi(s) = \EC{R_t}{s_t = s} =
  \EC{\sum_{k=0}^\infty \gamma^{t+k}r(s_{t+k}, a_{t+k}, s_{t+k+1})}{s_t = s} \label{eq:def_Vfunc}
\end{equation}
which represents the average discounted return that will be obtained
by following policy $\pi$ starting from state $s$.  Similarly, the
value of action $a$ in a given state $s$ under policy $\pi$ is
\begin{equation}
  \begin{aligned}
  Q^\pi(s, a) &= \EC{R_t}{s_t = s, a_t = a} \\ &=
  \EC{\sum_{k=0}^\infty \gamma^{t+k}r(s_{t+k}, a_{t+k}, s_{t+k+1})}{s_t = s, a_t = a} .
  \end{aligned}
  \label{eq:def_Qfunc}
\end{equation}









\section{Bellman's equation}\label{reinf:bellman}
Both value functions given in
(\ref{eq:def_Vfunc}~and~\ref{eq:def_Qfunc}) can be written recursively
for any policy as
\begin{align}
  V^\pi(s) &= \EC{r_{t+1} + \gamma V^\pi(s_{t+1})}{s_t=s} \label{eq:recursV}\\
  Q^\pi(s, a) &= \EC{r_{t+1} +
    \gamma \max_{a'} Q^\pi(s_{t+1}, a')}{s_t=s, a_t=a} . \label{eq:recursQ}
\end{align}
If the MDP is finite the existence of an optimal policy $\pi^*$ that
satisfies
\begin{alignat}{2}
  V^{\pi^*}(s) &= V^*(s) &&= \max_\pi V^\pi(s) \\
  Q^{\pi^*}(s, a) &= Q^*(s,a) &&= \max_\pi Q^\pi(s,a)
\end{alignat}
is ensured~\cite{sutton_reinforcement_2018}.  With an optimal policy
$\pi^*$, the state value function~\eqref{eq:recursV} becomes
\begin{equation}
  V^*(s) = \max_a \EC{r_{t+1} + \gamma V^*(s_{t+1})}
  {s_t=s, a_t = a} \label{eq:bellmanV}
\end{equation}
while the action value function~\eqref{eq:recursQ} remains unchanged
\begin{equation}
    Q^*(s, a) = \EC{r_{t+1} +
    \gamma \max_{a'} Q^*(s_{t+1}, a')}{s_t=s, a_t=a} . \label{eq:bellmanQ}
\end{equation}
In this form, the optimal value functions are \emph{Bellman optimality
  equations} \cite{bellman_markovian_1957},
which admit a unique solution.

If the MDP is finite and known, the Bellman optimality
equation~\eqref{eq:bellmanV} gives a linear system of
$\abs{\mathcal{S}}$ equations which could, in principle, be solved to
obtain the optimal state value function.  Unfortunately this approach
becomes unfeasible if the size of the state space is not small.

A well-known iterative algorithm that solves~\eqref{eq:bellmanV} is
\emph{value iteration}. This algorithm starts from arbitrary initial
values $V_0(s)$ and iteratively applies~\eqref{eq:bellmanV}:
\begin{equation}
  V_k(s) =
  \max_a \EC{r_{t+1} + \gamma V_{k-1}(s_{t+1})}{s_t = s, a_t = a}
  . \label{eq:Viter}
\end{equation}
If the optimal value function $V^*$ exists, then $V_k(s) \to V^*(s)$
as $k \to \infty$.
%
Exploiting the relation between state and action values ($V^\pi(s) =
\max_a Q^\pi(s,a)$), the update rule~\eqref{eq:Viter} can also be used
to find $Q^*$:
\begin{equation}
  Q_k(s, a) =
  \EC{r_{t+1} + \gamma \max_{a'} Q_{k-1}(s_{t+1}, a')}{s_t = s, s_t = a}
  . \label{eq:Qiter}
\end{equation}

The standard version of the value iteration algorithm sweeps the whole
state space at each iteration to compute $V_k(s)\ \forall s \in
\mathcal{S}$; this can make the algorithm very computationally
expensive if the state space is large. An
alternative~\cite{sutton_reinforcement_2018} is to use the same update
rules \emph{asynchronously}: instead of computing $V_k(s)\ \forall s$,
the value function is computed along the agent's path.

The asynchronicity makes it easier to implement the iterated updates
recursively. If the number of value iterations is fixed to $n$ and the
initial values are set to 0, the asynchronous action value iteration
can also be seen as an \emph{$n$-step greedy lookahead} planning
algorithm: the agent in state $s$ picks its next action in order to
maximize the discounted average reward over the next $n$ steps.

This $n$-step greedy lookahead algorithm will be used in the following
chapters to compare the results of three model-free \acl*{QL}-based
algorithms to an algorithm that has full knowledge of the MDP.

\section{Q-learning}\label{reinf:qlearn}
A famous model-free algorithm to solve MDPs is \ac{QL}. It is an
\emph{off-policy} \emph{temporal-difference} (TD) control algorithm
introduced by~\cite{watkins_learning_1989}.

The name comes from the objective of the algorithm, since it estimates
the optimal action value function $Q^*(s,a)$.
%
The TD class of algorithms is characterized by the use of
bootstrapping in the value function estimation: the estimate of
$Q_k(s,a)$ at any given time step is computed using the estimate
itself ($Q_{k-1}(s', a')$).

\acl{QL} uses the following update rule:
\begin{equation}
  Q_k(s,a) = (1 - \alpha(k,s,a)) Q_{k-1}(s,a) +
  \alpha(k,s,a)\left( r + \gamma \max_{a'}Q_{k-1}(s', a') \right)
  \label{eq:qlearn_update}
\end{equation}
which uses the transitions $(s, a, r, s')$ to update iteratively the
value function. At each new observed transition, the value of the
state-action pair is updated, while all the other values remain
unchanged.  The initial values $Q_0(s,a)$ can be set to be greater
than any discounted return, in order to ensure that unexplored actions
are preferred; if the rewards are bounded by $\bar{r} \geq
r_t\ \forall t$ then
\begin{equation}
  Q_0(s,a) = \bar{r} \sum_{k=0}^\infty \gamma^k = \frac{\bar{r}}{1 - \gamma} .
  \label{eq:qlearn_Q0)}
\end{equation}

The transitions can be controlled by any policy $\tilde{\pi}_k$, as
soon as the policy assigns a non-zero probability to every action
($\tilde{\pi}_k(s,a) > 0\ \forall k,s,a$); policy $\tilde{\pi}_k$ is
often derived from the value function at the corresponding time step
$Q_k$.

\acl{QL} is called an off-policy algorithm because it acts in the
environment to collect the transition samples according to a policy
($\tilde{\pi}_k$) that is different from the policy that the algorithm
evaluates and refines over time. The latter policy is the greedy
policy with respect to $Q_k$ (see~\eqref{eq:greedy_policy}).

The function $\alpha(k,s,a) \in [0,1]$, called learning rate, controls
the importance that the update rule gives to the observed reward
samples. A high learning rate makes the agent adjust rapidly $Q_k$ by
using the new reward $r$, but may cause instability. As will be seen
more precisely later, in order for the value function to converge to
the optimal one, the learning rate has to eventually go to zero.

Algorithm~\ref{alg:qlearn} describes \ac{QL} applied to multiple
episodes. If the task is not inherently episodic it can just be
stopped after a fixed number of steps.
%
\begin{algorithm}
  \caption{\acl*{QL} (adapted from~\cite{wiering_book_chap1})}
  \label{alg:qlearn}
  \begin{algorithmic}
    \Require discount rate $\gamma$, learning rate $\alpha(k,s,a)$,
    exploration policy $\tilde{\pi}_k(s,a)$
    \State initialize $Q(s,a)$ (e.g as in~\eqref{eq:qlearn_Q0)})
    \For{\textbf{each} episode}
    \State $s \gets$ starting state
    \State $k \gets 1$
    \Repeat
    \State choose $a \in \mathcal{A}(s)$ with probability $\tilde{\pi}_k(s, a)$
    \State make a transition from $s$ with action $a$
    \State let $r$ be the reward, $s'$ be the next state
    \State $Q(s,a) \gets (1 - \alpha(k,s,a)) Q(s,a) +
    \alpha(k,s,a)\left( r + \gamma \max_{a'}Q(s', a') \right)$
    \State $s \gets s'$
    \State $k \gets k+1$
    \Until{$s$ is terminal}
    \EndFor
  \end{algorithmic}
\end{algorithm}
%
Once the algorithm terminates, the estimated value function $Q$ can be
used to derive a greedy policy:
\begin{equation}
  \pi(s, a) = \ind{a = \argmax_{a'} Q(s,a')}
  . \label{eq:greedy_policy}
\end{equation}

According to the convergence proofs for \acl{QL} by
\cite{watkins_q-learning_1992, jaakkola_convergence_1994}, in order to
have $Q_k(s, a) \to Q^*(s, a)$ almost surely as $k \to \infty$, it is
sufficient that the rewards are bounded, that the learning rate is in
the interval $[0,1)$ and that
\begin{equation}
    \sum_{i=1}^\infty \alpha(k, s, a) = \infty, \quad
    \sum_{i=1}^\infty \alpha^2(k, s, a) < \infty, \quad
    \forall s,a
    .
    \label{eq:ql_cond}
\end{equation}
Assuming a bounded reward, the following choice of learning rate
satisfies the above conditions:
\begin{equation}
  \alpha(k, s, a) = \alpha(k) = \frac{\alpha_0}{k^{\alpha_\mrm{exp}}} ,
  \quad k > 0, \alpha_0 \in (0,1],
  \alpha_\mrm{exp} \in \left(0.5, 1\right],
  \forall s, a
  .
  \label{eq:ql_alpha}
\end{equation}

As will be seen later, \ac{QL} can yield reasonably good policies
even when applied directly over a POMDP, where convergence is not
guaranteed.

\section{Q-learning extensions}\label{reinf:qlearn_ext}
In order to improve the performance of \ac{QL} over multi-agent
problems, the authors of \cite{kaisers_frequency_2010} proposed an
extension, named \ac{FAQL}.  They showed
that the different frequency of the updates applied to each
state-action pair causes a discrepancy between the experimental
results from \ac{QL} and the dynamics predicted by evolutionary
game theory.
%
They located the cause of this discrepancy in the frequency of the
updates to $Q$, which is different for each state-action pair.  Their
proposed solution\footnote{ The original
  paper~\cite{kaisers_frequency_2010} only analyzes the case of a
  stateless two-agent stochastic game. Their approach is applied in
  this thesis to a stateful problem,
  following~\cite{abdallah_addressing_2016}.  } is to use a modified
learning rate, that takes into account the probability of selecting a
given action under the exploration policy $\tilde{\pi}_k$:
\begin{equation}
  \alpha_\mrm{FAQL}(k, s, a) = \alpha(k, s,a)
    \min\left(\frac{\beta(k, s, a)}{\tilde{\pi}_k(s, a)}, 1\right),
  \quad \forall s,a
  .
  \label{eq:faql_effective_alpha}
\end{equation}
This modified learning rate causes the value of state-action pairs
that occur less frequently to be adjusted more quickly.  The new
learning rate term must be clipped to 1 because $1/\tilde{\pi}_k(s,a)$
could go to infinity when the policy assigns low probabilities to some
actions.
%
The new parameter $\beta(k,s,a) \in (0, 1)$ controls the effect of the
policy on the learning rate: in order for the effective learning rate
$\alpha_\mrm{FAQL}$ to actually take into account the state-action
probability, $\tilde{\pi}_k(s,a) > \beta(k,s,a)$ must hold. This
implies that, ideally, $\beta$ should be as small as possible, because
\ac{FAQL} would otherwise reduce to standard \ac{QL}.\@ A small $\beta$,
however, can reduce the convergence speed, since it makes $Q$ update
slowly.


Since the \ac{FAQL} algorithm is just standard \ac{QL} with a different
learning rate, it can be implemented in the same way (see
Algorithm~\ref{alg:qlearn}) and is guaranteed to converge under the
same conditions.
%
Assuming $\beta(k,s,a) \in (0, 1), \forall k,s,a$,
\begin{equation}
  \beta(k, s, a) \leq \min\left(\frac{\beta(k,s,a)}{\tilde{\pi}_k(s,a)}, 1\right) \leq 1
\end{equation}
therefore
\begin{align}
  \sum_{i=1}^\infty \alpha_\mrm{FAQL}^2(k, s, a) &\leq
  \sum_{i=1}^\infty \alpha^2(k, s, a) \\
  \sum_{i=1}^\infty \alpha_\mrm{FAQL}(k, s, a) &\geq
  \sum_{i=1}^\infty \alpha(k, s, a)\beta(k, s, a)
  .
\end{align}
If, for instance, $\alpha$ is chosen like in \eqref{eq:ql_alpha} and
$\beta$ has the following similar shape:
\begin{equation}
  \beta(k, s, a) = \beta(k) = \frac{\beta_0}{k^{\beta_\mrm{exp}}} ,
  \quad k > 0, \beta_0 \in (0,1],
  \beta_\mrm{exp} \leq 1 - \alpha_\mrm{exp},
  \forall s, a
  \label{eq:faql_beta}
\end{equation}
then $\alpha_\mrm{FAQL}$ satisfies \eqref{eq:ql_cond} and the
convergence of \ac{QL} ensures that \ac{FAQL}, too, converges almost
surely to the optimal value function. The convergence proof holds,
like for \ac{QL}, if the MDP is finite and stationary.

More recently, the \ac{FAQL} algorithm was extended
by~\cite{abdallah_addressing_2016} to remove the additional parameter
$\beta$ and to avoid the degeneration to standard \ac{QL} when the
state-action frequency is low.  Their new algorithm is called
\ac{RUQL}.

The main idea behind \ac{RUQL} is to actually repeat the updates to the
value function a number of times inversely proportional to the
probability of choosing the action. This trades the unbounded value of
the update in ideal \ac{FAQL} for an unbounded execution time.
%
This ideal version of \ac{RUQL} can, however, be approximated by a single
update. The authors prove that the \ac{RUQL} update rule can be
approximated by the \ac{QL} update rule with the learning rate
\begin{equation}
  \alpha_\mrm{RUQL}(k, s, a) = 1 - (1 - \alpha(k, s, a))^\frac{1}
        {\tilde{\pi}_k(s, a)}
  \label{eq:ruql_effective_alpha}
\end{equation}
where the approximation introduces an error in $Q_k(s,a)$ of
$\bigO(\alpha^2(k,s,a))$.

The authors of \ac{RUQL}~\cite{abdallah_addressing_2016} also provide a
convergence proof for their algorithm similar to the one given above
for \ac{FAQL}.\@ Assuming that $\alpha$ satisfies~\eqref{eq:ql_cond}, the
same conditions require that
\begin{equation}
  \sum_{k=1}^\infty {\left( \frac{\alpha(k, s, a)}{\tilde{\pi}_k(s,a)}\right)}^2
    < \infty
  .
  \label{eq:ruql_cond}
\end{equation}
In order for the sum in~\eqref{eq:ruql_cond} to converge, the
exploration policy $\tilde{\pi}_k$ must not converge to a greedy
policy ``too fast''.  This condition can be trivially satisfied by
using an exploration policy where $\exists c(s,a) > 0$ such that
$\tilde{\pi}_k(s,a) \geq c(s,a)\ \forall k$.  An $\epsilon$-greedy policy
has this property and is defined as:
\begin{align}
  \tilde{\pi}_k(s,a) &= \begin{cases}
    1 - \epsilon(k) & \text{if}\; a = \argmax Q_k(s,a) \\
    \frac{\epsilon(k)}{\abs{\mathcal{A}(s)}-1} & \text{otherwise}
  \end{cases} \\
  \epsilon(k) &= \max\left(\epsilon_0 (1 - \epsilon_C k),
    \epsilon_\mrm{min}\right) \label{eq:epsilon}
\end{align}
where the lower bound is given by $c(s,a) =
\frac{\epsilon_\mrm{min}}{\abs{\mathcal{A}(s)}-1}$.  Similarly, the
softmax policy satisfies \eqref{eq:ruql_cond}, and is defined as:
\begin{align}
  \tilde{\pi}_k(s,a) &= \frac{e^\frac{Q_k(s,a)}{\tau(k)}}
    {\sum_{a' \in \mathcal{A}(s)} e^\frac{Q_k(s,a')}{\tau(k)}} \\
  \tau(k) &= \max\left(\tau_0 (1 - \tau_C k),
    \tau_\mrm{min}\right) \label{eq:tau}
\end{align}
where the lower bound, which is positive as soon as
$\sup_{k,s,a}\abs{Q_k(s,a)} = \bar{Q} < \infty$, is $c(s,a) =
\frac{1}{\abs{\mathcal{A}(s)}} e^{-\bar{Q}\left(\frac{1}{\tau_0} +
  \frac{1}{\tau_\mrm{min}}\right)}$.

In their paper, the authors of \ac{RUQL} tested their algorithm on both
stateless and stateful non-stationary problems.

\section{Monte Carlo Tree Search}\label{reinf:mcts}
Another reinforcement learning algorithm family used in the following
chapters is Monte Carlo Tree Search algorithms.  Algorithms of this
kind are often used as planning algorithms
\cite{sutton_reinforcement_2018}: they are run again each time the
agent needs to select an action from its current state, without
keeping memory between successive calls. Some MCTS variants choose to
reuse part of the tree they build, but this is not necessary in
general.

The main idea behind MCTS is to focus the planning along those paths
that appear to be of higher value, instead of exploring all reachable
states. Because of this they may be seen as a \emph{sparse} version of
the greedy lookahead algorithm, which has to actually consider every
possible (truncated) path that starts from the agent's state.

The basic structure of an MCTS algorithm
\cite{sutton_reinforcement_2018, browne_survey_2012} consists in
iterating the following four steps until a resource constraint is
reached, in terms of time or iteration count.
\begin{description}
\item[Selection] Starting from the root node, the tree is traversed
  following the \emph{tree policy} until a leaf is reached.
\item[Expansion] At the leaf node, a new child is added.
\item[Simulation] From the new child the \emph{default policy} (or
  \emph{rollout policy}) is applied to a simulated trajectory until a
  terminal state is reached. The terminal condition for continuing
  tasks can be e.g.\ a fixed depth.
\item[Backup] The result of the simulation is propagated toward the
  root to update the value function estimates.
\end{description}
The two kind of policies employed here differ in the information that
they can exploit. The tree policy can use the estimated values to
direct the path toward more promising children. The rollout policy,
instead, cannot know such information and typically assigns uniform
probabilities to the available actions.

The MCTS algorithm given above typically assumes that the transitions
are deterministic, given the action. If this is not the case, it can be
adapted with two modifications: the actions are represented with
additional nodes (with an associated value) and the selection phase
must simulate transitions too, in order to pick the child state node.



%% is described
%% for the case of deterministic transitions \cite{browne_survey_2012, }

%% The basic structure of an MCTS algorithm is shown in Algorithm~\ref{alg:mcts}.
%% \begin{algorithm}
%%   \centering
%%   \begin{algorithmic}
%%     \Function{MCTSSearch}{$s_0$}
%%     \State create root node $v_0$ with state $s_0$
%%     \Repeat
%%     \State $v_l \gets \mrm{TreePolicy}()
%%     \Until{Timeout}
%%     \State \textbf{return} BestAction(state, 0)
%%     \EndFunction

%%     \Function{Search}{state, depth}
%%     \IIf{Terminal(state)} \textbf{return} 0 \EndIIf
%%     \IIf{Leaf(state, depth)} \textbf{return} Evaluate(state) \EndIIf
%%     \State action $\gets$ SelectAction(state, depth)
%%     \State (nextstate, reward) $\gets$ SimulateAction(state, action)
%%     \State q $\gets$ reward + $\gamma$ Search(nextstate, depth + 1)
%%     \State UpdateValue(state, action, q, depth)
%%     \State \textbf{return} q
%%     \EndFunction
%%   \end{algorithmic}
%%   \caption{Basic MCTS
%%     (from~\cite{browne_survey_2012})}\label{alg:mcts}
%% \end{algorithm}
%% This algorithm works by repeatedly sampling episodes, starting from
%% the state whose value must be estimated. At each iteration, the
%% algorithm descends the tree formed by the states reachable from the
%% root, following a simulated trajectory and tracking the discounted
%% return gained along its path.  When the simulated path reaches a leaf
%% state is uses an evaluation function to use as an estimate of the
%% state value, similarly to the $V_0$ function in value iteration.  The
%% value function and discounted return are then propagated toward the
%% root, in order to update the value function estimates.

This algorithm is not entirely model-free, because it cannot learn
purely from experience; it requires, instead, a generative model to
simulate transitions starting from arbitrary points in the state
space. This can often be, as in this thesis, much faster than
explicitly computing the transition probabilities.

A major distinguishing element of each MCTS algorithm is the procedure
used to select the actions along the simulated trajectories.  A famous
MCTS algorithm is the Upper Confidence Bound applied to Trees (UCT)
\cite{hutchison_bandit_2006}.

\subsection{Upper Confidence Bound applied to Trees (UCT)}\label{reinf:uct}
This MCTS algorithm described by \cite{hutchison_bandit_2006} is an
adaptation of an algorithm for stateless decision problems: the Upper
Confidence Bound (UCB), which can be found in
\cite{sutton_reinforcement_2018}.

The UCB algorithm maintains an estimate of the value of each action
($Q_t(a)$) by averaging the rewards it received:
\begin{equation}
  Q_t(a) = (1 - \alpha) Q_{t-1}(a) + \alpha r_{t+1}
\end{equation}
and uses this value estimate to apply the following policy:
\begin{equation}
  a_t = \argmax_a\left( Q_t(a) + c\sqrt{\frac{\log t}{N_t(a)}} \right) ,
  \label{eq:ucb_action}
\end{equation}
where $N_t(a)$ is the number of times that action $a$ was chosen up to
time $t$ and $c > 0$ is a constant that tunes the exploration-exploitation
tradeoff.

The UCT algorithm follows the basic MCTS structure described above
and, in the selection phase, it considers each state node as a
separate stateless decision problem. It can then apply the same policy
from UCB~\eqref{eq:ucb_action} as tree policy:
\begin{equation}
  \pi_\mrm{tree}(s_t) =
  \argmax_a\left( Q_t(s, a, d) + c\sqrt{\frac{\log N_t(s, d)}{N_t(s, a, d)}}
  \right) ,
  \label{eq:uct_action}
\end{equation}
, where $Q$ becomes the action value estimate stored in the children
action nodes, $d$ is needed to distinguish the same state-action pair
occurring at different depths of the tree, $N_t(s, d)$ counts the
number of times the state node was visited and $N_t(s, a, d)$ counts
the number of times the action was chosen from the state node.
%
When an action was never chosen ($N_t(a) = 0$), it is assumed that its
value is infinite. This implies that the leaf nodes, from which the
search tree is expanded, are those with actions that were never
explored. After the tree policy uniformly selects one of the
unexplored actions, the rollout policy proceeds by uniformly selecting
actions until the termination condition is satisfied.
%
When the rollout terminates, the accumulated discounted return is used
as the value estimate associated with the new state node and is
propagated along its predecessors to update their values.

A small modification to UCT, called Sparse UCT, is proposed by
\cite{bjarnason_lower_2009}. The authors cap the number of children of
any action node to a fixed number $w$. The children nodes are added
like in regular UCT but, when the number of children reaches the
maximum value, the transitions are not generated by the MDP anymore;
instead, the next state is drawn uniformly from the $w$ existing
children.  The rest of the algorithm proceeds as before.

The rationale given in \cite{bjarnason_lower_2009} for the cap on the
number of children is that this approach reduces the expensive
sampling of new states.  This problem will also manifest in this
thesis so Sparse UCT is the algorithm that will be used in the
following chapters.

\section{\acf{MARL}}
There are multiple ways to extend the {(PO)MDP} framework to problems
where multiple agents must interact with each other and with the
environment to achieve some (shared or individual) goal.
%
A general model described by~\cite{littman_markov_1994} is that of
\emph{Markov games}, denoted by a tuple $(N, \mathcal{S},
\mathcal{A}_0, \dots, \mathcal{A}_{N-1}, T, r_0, \dots, r_{N-1})$ and where the
interaction of the agents with the environment happens in the
following way:
\begin{itemize}
  \item all the $N$ agents observe the state $s \in \mathcal{S}$ of
    the environment;
  \item they simultaneously select a joint action $\vec{a} =
    (a_0,\dots,a_{N-1}) \in \bigtimes_{i=0}^{N-1}\mathcal{A}_i(s)$ to carry out;
  \item the state of the environment transitions into a new state $s'
    \in \mathcal{S}$ according to the transition function $T:
    \mathcal{S} \times \mathcal{A}_0 \times \dots \times
    \mathcal{A}_{N-1} \times \mathcal{S} \to [0,1]$, which gives a
    conditional probability density function over $s'$ given $s$ and
    $\vec{a}$;
  \item each agent receives a separate reward $r_i$ according to the
    reward functions $r_i : \mathcal{S} \times \mathcal{A}_0 \times
    \dots \times \mathcal{A}_{N-1} \times \mathcal{S} \to \realset$.
\end{itemize}

The degree of cooperation between the agents can influence the way in
which the joint action is chosen at each decision time.
%
In~\cite{laurent_world_2011} \ac{MARL} problems are divided in two
fundamental classes: \emph{independent learner} algorithms and
\emph{joint action learner} algorithms.
%
The latter case is theoretically easier to deal with, because each
agent is aware of the actions and rewards of other agents, thus the
Markovianity holds from the point of view of each agent.
%
If the agents also have a common objective ($r_i = r\ \forall i$) then
the decision problem is usually called \emph{fully cooperative} game
and it can be reduced to a single-agent
MDP~\cite{busoniu_comprehensive_2008}, which can be solved using
single-agent RL algorithms.

The joint action learner setting requires, however, that the agents
are able to communicate between them and agree on a joint action to be
executed. Even in the case where the problem reduces to single-agent
MDP, the action space size scales exponentially and may cause the
problem to become untreatable.

The other case (independent learners) is that in which the agents are
completely unaware of the decisions taken by other agents, and can
only see the effect of their actions on the state. In this case they
need only to consider their own actions, avoiding the problems with
the size of the joint action space.

There are also intermediate cases between joint actions and
independent learners~\cite{boutilier_sequential_1999,
  busoniu_comprehensive_2008}, where the agents are provided with
explicit means of coordination, but they also have some degree of
independence.

A problem that almost always arises from the presence of multiple
independent agents is that each agent observes a non-Markovian
environment, because of the influence of the other agents'~actions.
%
The authors of~\cite{laurent_world_2011} investigate some sufficient
conditions for this: as soon as there exist two learning paths for an
agent that occur with non-zero probability and with a common final
state, other agents see the environment as non-Markovian.

When each agent receives its own reward, it is possible to define its
goal like for the single-agent case, e.g.\ as the maximization of the
discounted return (see~\eqref{eq:def_return}).
%
While an algorithm that employs some form of coordination will need a
global optimality criterion, if the agents are completely independent
the discounted returns criterion given above may be enough.

As will be seen in the following chapters, this thesis will apply this
second kind of \ac{MARL}. Specifically, the same algorithms used for the
single-agent POMDP will be applied independently by each agent.

%% It is possible,
%% however, that a policy that maxmizes the discounted return of all
%% agents simultaneously does not exist. In this case, a different
%% criterion is needed to measure the performance of the global system.
%% %
%% This global performance metric is not used in some cases, for instance
%% when the the agents are completelety independent,
